# Adding Test Data

Each file in `app/data/records` describes a complete record.

### Minimum data set

```json
{
  "nino": "AE797557B",
  "guid": "d4a53d603d1fb336ac20a962b35d1a33516dd31976f5afe4b29f73008a23804d",
  "dateOfBirth": "1999-08-27",
  "gender": "F",
  "forename": "Jhonny",
  "surname": "Root",
  "postcode": "L19 9DB",
  "contactDetails": [
    "9099014993"
  ],
  "CIS": {}
}
```

This record can be located in FINDr using the above information.

The `CIS` element is required but can be empty as above (in this
case then no KBV questions will be available).

Unit tests will validate the above and will tell you when something is wrong.

### CIS data

This record has `services` and `contactDetails` but no `relationships`

```json
  "CIS": {
    "services": "675,70",
    "contactDetails": [
      "1:01619516838",
      "3:07950456106"
    ],
    "personRoleRelationships": []
  }
```

Service `675` means the person has PIP (70 is Job Seekers Allowance)

Note that `contactDetails` in the `CIS` section is used only for KBV
questions, not for FINDr matching.

### ESA data

```json
  "ESA": {}
```

### PIP data

```json
  "PIP": {}
```

### Special Scenarios
