const express = require('express');
const parseData = require('./lib/parse-data');

const app = express();

app.disable('x-powered-by');

require('./lib/logger-middleware')(app);

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static('./app/public'));

// get the data from /app/data/records
const allData = parseData();

// control pages
require('./services/mock-info')(app, allData);
require('./services/mock-detail')(app, allData);
require('./services/mock-endpoints')(app, allData);
require('./services/mock-api-info')(app, allData);

// middleware to handle test scenarios goes here...
// slow responses, errors, specific responses
// TODO

// services
require('./services/iga-matching')(app, allData);
require('./services/iga-guid-service')(app, allData);
require('./services/dwp-guid-service')(app, allData);
require('./services/cis-api-014')(app, allData);
require('./services/cis-api-008')(app, allData);
require('./services/cis-api-009')(app, allData);
require('./services/alive-check')(app, allData);
require('./services/cis-api-038')(app, allData);
require('./services/pip')(app, allData);
require('./services/hmrc-mock')(app, allData);

// not found
app.use((req, res) => {
  console.log(404, req.method, req.path, req.headers, req.body); // eslint-disable-line no-console
  res.sendStatus(404);
});

// default express error handler
// eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => {
  console.error(error); // eslint-disable-line no-console
  res.sendStatus(500);
});

module.exports = app;
