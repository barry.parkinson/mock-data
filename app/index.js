const app = require('./app');

// const port = process.env.PORT;
const ports = [8080, 8082, 8083, 8084, 8085, 8086, 8088];
let running = 0;

ports.forEach((port) => {
  app.listen(port, () => {
    running++;
    if (running >= ports.length) {
      console.log(`IDT Mocks Services running on ports ${ports}`); // eslint-disable-line no-console
    }
  });
});
