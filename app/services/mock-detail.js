function render(req, res, allData) {
  const { ninoToPerson } = allData;
  const { nino } = req.params;
  const person = ninoToPerson.get(nino);
  if (person) {
    res.setHeader('content-type', 'text/plain');
    res.send(JSON.stringify(person, undefined, '  '));
  } else {
    res.send({
      nino,
      error: 'not found',
    });
  }
}

module.exports = (app, allData) => {
  app.get('/mock-detail/nino/:nino', (req, res) => {
    render(req, res, allData);
  });
};
