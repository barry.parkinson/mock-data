// award history api
const { registerEndpoint } = require('../lib/register-endpoint');

const url = '/cis/get/api-008-shared';

registerEndpoint({
  id: 'cis-api-008',
  description: 'CIS API-008 Contact Details',
  method: 'GET',
  url,
  headers: {
    nino: 'AE767818',
    sessionID: '{{sessionId}}',
  },
  headerNotes: ['nino is the first 8 characters (without the suffix)', 'standard CIS headers also required'],
});

module.exports = (app, allData) => {
  const { shortNinoToPerson } = allData;

  app.get(url, (req, res) => {
    const nino = req.get('nino');
    const person = shortNinoToPerson.get(nino);

    if (person && person.CIS && person.CIS.contactDetails) {
      const contactDetails = person.CIS.contactDetails.map((d) => {
        const [contactTypeID, detail] = d.split(':');
        return {
          contactTypeID,
          detail,
        };
      });
      res.send({
        GetContactDetailsResponse: {
          header: [{
            natSensInd: '1',
          }],
          contactDetails,
        },
      });
    } else {
      res.send({ error: 'NOT FOUND' });
    }
  });
};
