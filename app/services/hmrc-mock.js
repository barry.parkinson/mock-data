/* eslint-disable max-len */
const express = require('express');
const makeUuid = require('../lib/make-uuid');
const sendAndLog = require('../lib/sendAndLog');
const { registerEndpoint } = require('../lib/register-endpoint');

registerEndpoint({
  id: 'hmrc-search',
  description: 'HMRC Retrieve',
  method: 'POST',
  url: '/individuals/iv-orchestration/iv-sessiondata/search',
});

registerEndpoint({
  id: 'hmrc-verify',
  description: 'HMRC Uplift',
  method: 'POST',
  url: '/prove-your-identity/standalone-verification',
});

module.exports = (app, allData) => {
  const urlEncoded = express.urlencoded({ extended: false });
  const { ninoToPerson } = allData;

  app.get('/someurl', (req, res) => { // postman tests redirect here - ignore
    res.send('ok');
  });

  // GET on this url should return a HTML page

  app.get('/prove-your-identity/standalone-verification', (req, res) => {
    const { continueUrl } = req.query;
    // TODO where is the page?
    res.send(`<html> Identity Verification Stub ${continueUrl}`);
  });

  // POST to this with a journeyId in the body
  // journeyId is a hex string containing nino + evidence count + outcome
  // the response contains PII data nino, names, dob etc and confidence level

  app.post('/individuals/iv-orchestration/iv-sessiondata/search', (req, res) => {
    const { journeyId } = req.body;
    // need to decode journeyId to get nino & evidencesPassedCount
    const decoded = makeUuid.decode(journeyId);
    const arr = decoded.split('-');
    const nino = arr[0];
    const ecc = parseInt(arr[1], 10);
    const out = arr[2];

    const person = ninoToPerson.get(nino);

    // not found ie AA000000A
    if (!person) {
      sendAndLog(req, res, 'hmrc-search', 200, {
        confidenceLevel: 250,
        ivFailureReason: 'TechnicalIssue',
      }, {
        journeyId,
        decoded,
      });
      return;
    }

    const response = {
      nino,
      postCode: person.postcode,
      firstName: person.forename,
      lastName: person.surname,
      dateOfBirth: person.dateOfBirth,
      confidenceLevel: out === 'S' ? 250 : 50,
      ivFailureReason: out === 'S' ? 'Success' : 'User failed IV',
      evidencesPassedCount: ecc, // 0 1 2
    };
    sendAndLog(req, res, 'hmrc-search', 200, response, {
      journeyId,
      decoded,
    });
  });

  // POST to this will redirect to continueUrl with a journeyId parameter
  // to mock this, the journeyId is a hex string containing nino + evidence count + outcome

  app.post('/prove-your-identity/standalone-verification', urlEncoded, (req, res) => {
    const { continueUrl } = req.query;
    const {
      outcome, // 'Success' or 'FailedIV'
      evidences, // 'passport,p60',
      nino, // 'RJ428577B',
    } = req.body;
    let ecc = evidences ? 1 : 0;
    if (evidences.indexOf(',') > 0) {
      ecc = 2;
    }
    const out = outcome.substring(0, 1);
    const journeyId = makeUuid.encode(`${nino}-${ecc}-${out}-   `);
    req.logger.info({
      api: 'hmrc-verify',
      status: 302,
      request: {
        continueUrl,
        outcome,
        evidences,
        nino,
        evidenceCount: ecc,
      },
      response: {
        journeyId,
      },
    });
    res.redirect(`${continueUrl}?journeyId=${journeyId}`);
  });

  app.post('/oauth/token', (req, res) => {
    res.send({
      access_token: '88d34d697fd46d0950367e880fa5a80b',
      scope: 'iv-orchestration',
      expires_in: 14400,
      token_type: 'bearer',
    });
  });
};
