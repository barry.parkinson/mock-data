// alive check api
const { registerEndpoint } = require('../lib/register-endpoint');
const sendAndLog = require('../lib/sendAndLog');
const validShortNino = require('../lib/validShortNino');

const url = '/integration/aliveCheck-shared';

registerEndpoint({
  id: 'alive-check',
  description: 'Alive Check',
  method: 'GET',
  url,
  headers: {
    nino: 'AE767826',
    correlationId: '9dd116d0-d85a-4f4b-9ff8-da06b29bfa15',
    sessionId: '{{sessionId}}',
    userID: 'EXT-IDNOUSER',
    userName: 'EXT-ID_NOUSER',
    systemID: '752',
    subSystemID: 'ExtIDIDV',
    hostName: '{{hostname}}',
    accessLevel: '4',
    departmentID: '1',
    officeID: '1',
    locationAddress: 'Sheffield',
    auditFlag: 'TRUE',
    'X-CertificateOuid': 'extnl-ID',
    'X-swimlaneId': '03',
  },
  headerNotes: ['nino is the first 8 characters (without the suffix)'],
});

module.exports = (app, allData) => {
  const { shortNinoToPerson } = allData;

  app.get(url, (req, res) => {
    const nino = req.get('nino');
    const send = (status, response) => sendAndLog(req, res, 'alive-check', status, response, { nino });
    const correlationId = req.get('correlationId');

    if (!validShortNino(nino)) {
      send(422, {
        correlationId,
        error: {
          errors: [{
            code: '2',
            message: 'Supplied NINO is invalid - format must be AA999999.',
          }],
          httpStatusCode: '422',
          message: 'Validation error(s)',
        },
      });
      return;
    }

    const person = shortNinoToPerson.get(nino);

    if (!person) { // NINO is not in the list
      send(404, {
        error: {
          httpStatusCode: '404',
          message: 'NINO Not Found',
        },
      });
      return;
    }

    if (person.aliveCheck === 'fail') {
      send(200, {
        correlationId,
        data: {
          isAlive: false,
          death: {
            date: '2011-10-11',
            description: 'Verified to Level 3',
            verification: 3,
          },
        },
      });
    } else {
      send(200, {
        correlationId,
        data: { isAlive: true },
      });
    }
  });
};
