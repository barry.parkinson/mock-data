// award history api
const { registerEndpoint } = require('../lib/register-endpoint');
const apiResponse = require('../data/responses/cisApi014Response');

const url = '/cis/get/api-014-shared';

registerEndpoint({
  id: 'cis-api-014',
  description: 'CIS API-014 Award History',
  method: 'GET',
  url,
  headers: {
    nino: 'AE767818',
    sessionID: '{{sessionId}}',
    userName: 'EXT-ID_NOUSER',
    userID: 'EXT-IDNOUSER',
    systemID: 752,
    subSystemID: 'ExtIDIDV',
    hostName: '{{hostname}}',
    locationAddress: 'Sheffield',
    officeID: '6012',
    departmentID: 1,
    accessLevel: '4',
    auditFlag: true,
    nationalSensitivityAccess: false,
    'X-CertificateOuid': 'extnl-id',
    'X-swimlaneId': '03',
    dwpServiceKeys: '85',
    historyFlag: true,
  },
  headerNotes: ['nino is the first 8 characters (without the suffix)'],
});

module.exports = (app, allData) => {
  const { shortNinoToPerson } = allData;

  app.get(url, (req, res) => {
    const nino = req.get('nino');
    const person = shortNinoToPerson.get(nino);
    if (person) {
      res.send(apiResponse(person));
    } else {
      res.send({ err: 'NOT FOUND' });
    }
  });
};
