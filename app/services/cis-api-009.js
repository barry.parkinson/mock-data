// award history api
const { registerEndpoint } = require('../lib/register-endpoint');

const url = '/cis/get/api-009-shared';

registerEndpoint({
  id: 'cis-api-009',
  description: 'CIS API-009 Relationship Details',
  method: 'GET',
  url,
  headers: {
    nino: 'AE767818',
    sessionID: '{{sessionId}}',
  },
  headerNotes: ['nino is the first 8 characters (without the suffix)', 'standard CIS headers also required'],
});

module.exports = (app, allData) => {
  const { shortNinoToPerson } = allData;

  app.get(url, (req, res) => {
    const nino = req.get('nino');
    const person = shortNinoToPerson.get(nino);
    if (person && person.CIS && person.CIS.personRoleRelationships) {
      const { personRoleRelationships } = person.CIS;
      res.send({
        GetRelationshipDetailsResponse: {
          header: [{
            natSensInd: '1',
          }],
          personRoleRelationships,
        },
      });
      return;
    }

    // no person or no CIS
    res.status(404);
    res.send({ error: 'NF' }); // TODO proper error
  });
};
