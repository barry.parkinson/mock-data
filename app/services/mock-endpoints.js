const { version } = require('../../package.json');
const { endpoints } = require('../lib/register-endpoint');

// return HTML describing mock data available

function jsonToHtml(obj, notes) {
  const a = [obj ? `<code>${JSON.stringify(obj, undefined, ' ')}</code>` : ''];
  if (notes) {
    notes.forEach((note) => {
      a.push('<ul>');
      a.push(`<li>${note}</li>`);
      a.push('</ul>');
    });
  }
  return a.join('');
}

function render(req, res) {
  const page = [];
  const add = (t) => page.push(t);
  const td = (text) => {
    add('<td>');
    add(text);
    add('</td>');
  };

  add('<html>');
  add('<head>');
  add('<link rel="stylesheet" href="app.css">');
  add('</head>');
  add('<body>');
  add(`<h1>Mock Data v${version} - Endpoints</h1>`);
  add('<table>');
  add('<tr><th>API</th><th>Headers</th><th>Body</th></tr>');
  endpoints.forEach((row) => {
    add('<tr>');
    td(`<b>${row.description}</b><br><br><a href="/api/info/${row.id}">${row.method} ${row.url}</a><br>&nbsp;`);
    td(jsonToHtml(row.headers, row.headerNotes));
    td(jsonToHtml(row.body, row.bodyNotes));
    add('</tr>');
  });
  add('</table>');

  add('<p><a href="/mock-info">View Mock Data</a>');

  add('</body>');
  add('</html>');

  // done
  const html = page.join('');
  res.set('content-type', 'text/html');
  res.send(html);
}

module.exports = (app, allData) => {
  app.get('/mock-endpoints', (req, res) => {
    render(req, res, allData);
  });
};
