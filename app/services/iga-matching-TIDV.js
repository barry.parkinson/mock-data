// TIDV policy described
// https://dwpdigital.atlassian.net/wiki/spaces/FINDR/pages/132563304948/POLICY+ID+13
const { registerEndpoint } = require('../lib/register-endpoint');
const sendAndLog = require('../lib/sendAndLog');

const url = '/match/';

registerEndpoint({
  id: 'match-tidv',
  description: 'TIDV FINDr Matching',
  method: 'POST',
  url,
  headers: {
    'policy-id': 'POLICY_13',
    consumer: 'CONSUMER_5',
    subSystemId: 'DTH-TIDV',
    userName: 'DTH-TIDV PIP Telephone Enquiry',
    'correlation-id': '',
  },
  body: {
    contactDetails: ['9099014821', '02071882329'],
    dateOfBirth: '1987-01-06',
    postcode: 'LS12 3NH',
    nino: '7822',
  },
  bodyNotes: [
    'contactDetails is required',
    'nino is the last four digits (for AE767822C, enter 7822)',
  ],
});

function matchContactDetails(cd1, cd2) {
  return cd1 && cd2 && cd2.find((phone) => cd1.includes(phone.toLowerCase()));
}

function match(s1, s2) {
  return s1 && s2 && s1.toLowerCase() === s2.toLowerCase();
}

function partMatch(s1, s2) { // ie LS12 3NH vs LS12 3XX
  return (s1 && s2 && match(s1.substring(0, 6), s2.substring(0, 6)));
}

function ninoMatch(fullNino, partNino) { // AE767822C match 7822
  return fullNino.indexOf(partNino) === 4;
}

function matchTIDV(req, res, personList) {
  const send = (status, response) => sendAndLog(req, res, 'match-tidv', status, response, req.body);
  const {
    contactDetails,
    dateOfBirth,
    postcode,
    nino,
  } = req.body;

  // BAD REQUEST
  if (!contactDetails || !contactDetails.length || (!dateOfBirth && !postcode)) {
    send(400, {
      statusCode: 400,
      status: 'BAD_REQUEST',
      message: 'Insufficient information supplied for match using policy: POLICY_13',
    });
    return;
  }

  // TODO too many results (error '1')

  let requestPostCode;
  let requestNino;
  let sc2;
  let sc3;
  const sc1 = personList.find((person) => {
    if (matchContactDetails(person.contactDetails, contactDetails)) {
      if (match(person.dateOfBirth, dateOfBirth)) {
        return true; // SCENARIO_1 contact + dob
      }
      if (!sc2) {
        if (match(person.postcode, postcode)) {
          sc2 = person; // SCENARIO_2 contact + postcode
          return false;
        }
        if (!sc3) {
          const partPostCode = partMatch(person.postcode, postcode);
          if (partPostCode && ninoMatch(person.nino, nino)) {
            sc3 = person; // SCENARIO_3 contact + nino + partial postcode
            return false;
          }
          if (!requestPostCode) {
            if (partMatch(person.dateOfBirth, dateOfBirth)) {
              requestPostCode = true; // contact + partial dob
              return false;
            }
            if (partPostCode) {
              requestNino = true; // contact + partial postcode
            }
          }
        }
      }
    }
    return false;
  });

  if (sc1) {
    send(200, {
      matchingScenario: 'SCENARIO_1',
      identifierValue: sc1.guid,
    });
    return;
  }

  if (sc2) {
    send(200, {
      matchingScenario: 'SCENARIO_2',
      identifierValue: sc2.guid,
    });
    return;
  }

  if (sc3) {
    send(200, {
      matchingScenario: 'SCENARIO_3',
      identifierValue: sc3.guid,
    });
    return;
  }

  if (requestPostCode) {
    send(422, {
      statusCode: 422,
      status: 'UNPROCESSABLE_ENTITY',
      error: '2',
      message: 'Further details required. POSTCODE',
      attributeEnums: ['POSTCODE'],
      loggingIdentifierValue: '25f7134d-f5b5-48d0-83bb-bd9f9a98f88c',
    });
    return;
  }

  if (requestNino) {
    send(422, {
      statusCode: 422,
      status: 'UNPROCESSABLE_ENTITY',
      error: '2',
      message: 'Further details required. CONTACT_DETAILS NINO',
      attributeEnums: [
        'CONTACT_DETAILS',
        'NINO',
      ],
      loggingIdentifierValue: '25f7134d-f5b5-48d0-83bb-bd9f9a98f88c',
    });
    return;
  }

  // NOT FOUND
  send(422, {
    statusCode: 422,
    status: 'UNPROCESSABLE_ENTITY',
    error: '0',
    message: 'No results found.',
    loggingIdentifierValue: '61b14ee9-1b8a-4e02-80c9-7402711125e1',
  });
}

module.exports = matchTIDV;
