const { version } = require('../../package.json');

// return HTML describing mock data available

function render(req, res, allData) {
  const { personList } = allData;
  const page = [];
  const add = (t) => page.push(t);
  const td = (text) => {
    add('<td>');
    add(text);
    add('</td>');
  };
  const tdGuid = (text) => {
    add('<td class="guid">');
    add(text);
    add('</td>');
  };

  add('<html>');
  add('<head>');
  add('<link rel="stylesheet" href="app.css">');
  add('</head>');
  add('<body>');
  add(`<h1>Mock Data v${version}</h1>`);
  add('<table>');
  add('<tr><th>NINO</th><th>Forename</th><th>Surname</th><th>DOB</th><th>Postcode</th><th>DWP GUID</th><th>Contact Details</th></tr>');
  personList.forEach((row) => {
    add('<tr>');
    td(`<a href="/mock-detail/nino/${row.nino}">${row.nino}</a>`);
    td(row.forename);
    td(row.surname);
    td(row.dateOfBirth);
    td(row.postcode);
    tdGuid(row.guid);
    td(JSON.stringify(row.contactDetails));
    add('</tr>');
  });
  add('</table>');
  add('<p><a href="/mock-endpoints">View Mock Endpoints</a>');
  add('</body>');
  add('</html>');

  // done
  const html = page.join('');
  res.set('content-type', 'text/html');
  res.send(html);
}

module.exports = (app, allData) => {
  app.get('/mock-info', (req, res) => {
    render(req, res, allData);
  });
};
