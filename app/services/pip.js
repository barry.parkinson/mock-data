const express = require('express');

function extractNino(text) {
  const rx = /<NINO>(.+?)<\/NINO>/;
  const result = text.match(rx);
  return result ? result[1] : undefined;
}

module.exports = (app) => {
  function handlePIP(req, res) {
    const { body } = req;
    // console.log('BODY', body, body.toString);
    // note malformed requests may have an object with a null prototype
    const text = (body && body.toString) ? body.toString() : '';
    // eslint-disable-next-line no-console
    console.log('PIP', req.method, req.path, req.headers, text);
    res.sendStatus(404);
    // eslint-disable-next-line no-console
    console.log('NINO', extractNino(text));
  }

  const raw = express.raw({ type: 'application/xml' });
  app.post('/PIPGetEntitlement', raw, handlePIP);
  app.post('/PIPGetPaymentDetails', raw, handlePIP);
  app.post('/PIPGetPersonalNewDetails', raw, handlePIP);
};
