const { findById } = require('../lib/register-endpoint');

function render(req, res) {
  const api = findById(req.params.id);
  if (!api) {
    res.sendStatus(404);
    return;
  }

  const page = [];
  const add = (t) => page.push(t);

  add('<html>');
  add('<head>');
  add('<link rel="stylesheet" href="/app.css">');
  add('</head>');
  add('<body>');
  add(`<h1>Endpoint: ${api.description}</h1>`);

  add(`<p>${api.method} ${api.url}`);

  add(`<p>ID: ${api.id}`);

  if (api.headers) {
    add(`<p>Headers:</p><code>${JSON.stringify(api.headers, undefined, ' ')}</code>`);
  }
  if (api.body) {
    add(`<p>Body:</p><code>${JSON.stringify(api.body, undefined, ' ')}</code>`);
  }

  add('<hr><p><a href="/mock-info">View Mock Data</a>');
  add(' | <a href="/mock-endpoints">View Mock Endpoints</a>');

  add('</body>');
  add('</html>');

  // done
  const html = page.join('');
  res.set('content-type', 'text/html');
  res.send(html);
}

module.exports = (app, allData) => {
  app.get('/api/info/:id', (req, res) => {
    render(req, res, allData);
  });
};
