const sendAndLog = require('../lib/sendAndLog');
const matchOIDV = require('./iga-matching-OIDV');
const matchTIDV = require('./iga-matching-TIDV');

module.exports = (app, allData) => {
  const { personList } = allData;

  app.post('/match/', (req, res) => {
    const policy = req.get('policy-id');
    if (policy === 'POLICY_12') {
      matchOIDV(req, res, personList);
    } else if (policy === 'POLICY_13') {
      matchTIDV(req, res, personList);
    } else {
      sendAndLog(req, res, 'matching', 400, {
        statusCode: 400,
        status: 'BAD_REQUEST',
        message: 'Unknown Policy Id supplied in headers',
      }, { policy });
    }
  });
};
