// ID at Risk api
const cisApi0038FailResponse = require('../data/responses/cisApi0038FailResponse');
const cisFaultInvalidShortNino = require('../data/responses/cisFaultInvalidShortNino');
const { registerEndpoint } = require('../lib/register-endpoint');
const sendAndLog = require('../lib/sendAndLog');
const validShortNino = require('../lib/validShortNino');

const url = '/cis/get/api-038-shared';

registerEndpoint({
  id: 'cis-api-038',
  description: 'CIS API-038 ID at Risk',
  method: 'GET',
  url,
  headers: {
    nino: 'AE767818',
    sessionID: '{{sessionId}}',
    userName: 'EXT-ID_NOUSER',
    userID: 'EXT-IDNOUSER',
    systemID: 752,
    subSystemID: 'ExtIDIDV',
    hostName: '{{hostname}}',
    locationAddress: 'Sheffield',
    officeID: '6012',
    departmentID: 1,
    accessLevel: '4',
    auditFlag: true,
    nationalSensitivityAccess: false,
    'X-CertificateOuid': 'extnl-id',
    'X-swimlaneId': '03',
    dwpServiceKeys: '85',
    historyFlag: true,
  },
  headerNotes: ['nino is the first 8 characters (without the suffix)'],
});

module.exports = (app, allData) => {
  const { shortNinoToPerson } = allData;

  app.get(url, (req, res) => {
    const nino = req.get('nino');
    const send = (status, response, log) => sendAndLog(req, res, 'cis-api-038', status, response, { nino }, log);

    if (!validShortNino(nino)) {
      send(400, cisFaultInvalidShortNino);
      return;
    }

    const person = shortNinoToPerson.get(nino);

    if (person && person.idAtRiskCheck === 'fail') { // only fail specific records
      send(200, cisApi0038FailResponse(nino));
      return;
    }

    send(200, { state: 'not-at-risk' }); // TODO ok response
  });
};
