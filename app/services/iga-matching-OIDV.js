// POST /match/
// body: contactDetails, postcode, nino: nino ? nino.substring(nino.length - 4), dateOfBirth,
// headers: policy-id = POLICY_13 for TIDV, POLICY_12 for OIDV
const { registerEndpoint } = require('../lib/register-endpoint');
const sendAndLog = require('../lib/sendAndLog');

const url = '/match/';

registerEndpoint({
  id: 'match-oidv',
  description: 'OIDV WEB FINDr Matching',
  method: 'POST',
  url,
  headers: {
    'policy-id': 'POLICY_12',
  },
  body: {
    dateOfBirth: '1967-11-23',
    forename: 'Kyle',
    surname: 'Scoby',
    postcode: 'TQ1 3UD',
    contactDetails: ['01610001234', 'scoby@gmail.com'],
  },
});

function matchContactDetails(cd1, cd2) {
  return cd2 && cd2.find((phone) => cd1.includes(phone.toLowerCase()));
}

function match(s1, s2) {
  return s1 && s2 && s1.toLowerCase() === s2.toLowerCase();
}

function matchOIDV(req, res, personList) {
  const send = (status, response) => sendAndLog(req, res, 'match-oidv', status, response, req.body);
  const {
    dateOfBirth,
    forename,
    surname,
    postcode,
    contactDetails,
  } = req.body;

  // SCENARIO_EXACT = forename + surname + dob + postcode
  const exact = personList.find((person) => match(person.forename, forename)
    && match(person.surname, surname)
    && match(person.dateOfBirth, dateOfBirth)
    && match(person.postcode, postcode));
  if (exact) {
    send(200, {
      matchingScenario: 'SCENARIO_EXACT',
      identifierValue: exact.guid,
    });
    return;
  }

  // SCENARIO_1 = surname + dob + postcode
  const scenario1 = personList.find((person) => match(person.surname, surname)
    && match(person.dateOfBirth, dateOfBirth)
    && match(person.postcode, postcode));
  if (scenario1) {
    send(200, {
      matchingScenario: 'SCENARIO_1',
      identifierValue: scenario1.guid,
    });
    return;
  }

  // SCENARIO_2 = forename + dob + postcode
  const scenario2 = personList.find((person) => match(person.forename, forename)
    && match(person.dateOfBirth, dateOfBirth)
    && match(person.postcode, postcode));
  if (scenario2) {
    send(200, {
      matchingScenario: 'SCENARIO_2',
      identifierValue: scenario2.guid,
    });
    return;
  }

  // SCENARIO_3 = surname + forename + dob
  const scenario3 = personList.find((person) => match(person.forename, forename)
    && match(person.surname, surname)
    && match(person.dateOfBirth, dateOfBirth));
  if (scenario3) {
    send(200, {
      matchingScenario: 'SCENARIO_3',
      identifierValue: scenario3.guid,
    });
    return;
  }

  // SCENARIO_4 = surname + dob + contact
  const scenario4 = personList.find((person) => match(person.surname, surname)
    && match(person.dateOfBirth, dateOfBirth)
    && matchContactDetails(person.contactDetails, contactDetails));
  if (scenario4) {
    send(200, {
      matchingScenario: 'SCENARIO_4',
      identifierValue: scenario4.guid,
    });
    return;
  }

  // SCENARIO_5 = forename + dob + contact
  const scenario5 = personList.find((person) => match(person.forename, forename)
    && match(person.dateOfBirth, dateOfBirth)
    && matchContactDetails(person.contactDetails, contactDetails));
  if (scenario5) {
    send(200, {
      matchingScenario: 'SCENARIO_5',
      identifierValue: scenario5.guid,
    });
    return;
  }

  // 422 request postcode = surname + dob
  const furtherInfo = personList.find((person) => match(person.surname, surname)
    && match(person.dateOfBirth, dateOfBirth));
  if (furtherInfo) {
    send(422, {
      statusCode: 422,
      status: 'UNPROCESSABLE_ENTITY',
      error: '2',
      message: 'Further details required. POSTCODE',
      attributeEnums: ['POSTCODE'],
      loggingIdentifierValue: '51b14ee9-1b8a-4e02-80c9-7402711125e1',
    });
  } else {
    send(422, {
      statusCode: 422,
      status: 'UNPROCESSABLE_ENTITY',
      error: '0',
      message: 'No results found.',
      loggingIdentifierValue: '51b14ee9-1b8a-4e02-80c9-7402711125e1',
    });
  }
}

module.exports = matchOIDV;
