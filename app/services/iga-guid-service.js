// stub for integrated-gateway-adapter, guid service
// route /guid-service
const { registerEndpoint } = require('../lib/register-endpoint');
const sendAndLog = require('../lib/sendAndLog');

const url = '/guid-service';

registerEndpoint({
  id: 'iga-guid-service',
  description: 'Guid Service (Integrated Gateway Adapter)',
  method: 'GET',
  url,
  headers: {
    'dwp-guid': 'dbfba70f83735c0....5c2d4296b46f69b1da3',
    'client-id': 'CxP-ESA-TIDV',
    'session-id': '',
    'correlation-id': '',
  },
});

module.exports = (app, allData) => {
  const { guidToNino } = allData;

  app.get(url, (req, res) => {
    const guid = req.get('dwp-guid');
    const send = (status, response) => sendAndLog(req, res, 'iga-guid-service', status, response, { guid });
    const nino = guidToNino.get(guid);

    if (nino) {
      send(200, {
        data: {
          identifier: nino,
          type: 'NINO',
        },
      });
    } else {
      send(422, {
        status: '422 UNPROCESSABLE_ENTITY',
        messages: [
          'Supplied DWP_GUID not found',
        ],
      });
    }
  });
};
