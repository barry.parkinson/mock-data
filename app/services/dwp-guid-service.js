// the proper guid service, not iga
const { registerEndpoint } = require('../lib/register-endpoint');
const sendAndLog = require('../lib/sendAndLog');
const validFullNino = require('../lib/validFullNino');

registerEndpoint({
  id: 'dwp-guid-from-nino',
  description: 'DWP Guid Service - get Guid from NINO',
  method: 'GET',
  url: '/dwp-guid-service/v0.2/DWP-GUID/for/NINO/:nino',
});

registerEndpoint({
  id: 'dwp-nino-from-guid',
  description: 'DWP Guid Service - get NINO from Guid',
  method: 'GET',
  url: '/dwp-guid-service/v0.2/NINO/for/DWP-GUID/:guid',
});

module.exports = (app, allData) => {
  const {
    guidToNino,
    ninoToPerson,
  } = allData;

  // convert a NINO to a DWP GUID

  app.get('/dwp-guid-service/v0.2/DWP-GUID/for/NINO/:nino', (req, res) => {
    const { nino } = req.params;
    const send = (status, response) => sendAndLog(req, res, 'dwp-guid-from-nino', status, response, { nino });

    if (!validFullNino(nino)) {
      send(400, {
        status: '400 BAD_REQUEST',
        messages: [
          'Input identifier not valid',
        ],
      });
      return;
    }

    const person = ninoToPerson.get(nino);
    if (person) {
      send(200, {
        type: 'DWP_GUID',
        identifier: person.guid,
      });
    } else {
      send(422, {
        status: '422 UNPROCESSABLE_ENTITY',
        messages: [
          'Supplied NINO not found',
        ],
      });
    }
  });

  // convert a DWP GUID to a NINO

  app.get('/dwp-guid-service/v0.2/NINO/for/DWP-GUID/:guid', (req, res) => {
    const { guid } = req.params;
    const send = (status, response) => sendAndLog(req, res, 'dwp-nino-from-guid', status, response, { guid });

    if (!guid.match(/^([A-Fa-f0-9]{64})$/)) {
      send(400, {
        status: '400 BAD_REQUEST',
        messages: [
          'Input identifier not valid',
        ],
      });
      return;
    }

    const nino = guidToNino.get(guid);
    if (nino) {
      send(200, {
        type: 'NINO',
        identifier: nino,
      });
    } else {
      send(422, {
        status: '422 UNPROCESSABLE_ENTITY',
        messages: [
          'Supplied DWP_GUID not found',
        ],
      });
    }
  });
};
