module.exports = {
  Fault: {
    faultcode: 'p264:CISFaultDetails',
    faultstring: 'uk.gov.dwp.cis.common.messaging.v2_0.CISFaultDetails',
    detail: [
      {
        CISFaultDetails: [
          {
            cisErrors: [
              {
                errorCode: '3',
                errorMessage: 'Invalid NINO - format must be AA999999. NINO prefix should be one of allowed values. Refer to the Corporate Data standard for a list of allowed values.',
              },
            ],
          },
        ],
      },
    ],
  },
};
