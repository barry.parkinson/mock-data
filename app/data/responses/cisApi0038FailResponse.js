module.exports = (nino) => ({
  getPersonResponse: {
    header: [
      {
        natSensInd: '1',
        mgtChkInd: '2',
      },
    ],
    person: [
      {
        ninoGroup: [
          {
            nino,
            ninoSuffix: 'A',
            idVerificationTypeID: '7',
            idVerificationTypeDesc: 'Verified',
          },
        ],
        birthDateGroup: [
          {
            birthDate: '15-Jan-1980',
            birthDateStartDt: '31-Jul-2017',
            birthDateGrpStartDtApplDt: '31-Jul-2017',
            birthDateGrpChgMdBenSysID: '590',
            birthDateGrpChgMdBenSysDesc: 'SEF',
            birthDateGrpChgMdOfceLocnID: '12309',
            birthDateGrpChgMdOfceTelNo: 'Details not available',
            birthDateGrpChgMdDeptID: '1',
            birthDateGrpChgMdDeptDesc: 'Department for Work and Pensions',
            birthDateVerificationFlagID: '2',
            birthDateVerificationFlagDesc: 'Verified to Level 2',
          },
        ],
        sexGroup: [
          {
            sex: 'M',
            sexStartDate: '31-Jul-2017',
            sexStartDateApplDt: '31-Jul-2017',
            sexChgMdBenSysID: '590',
            sexChgMdBenSysDesc: 'SEF',
            sexChgMdOfceLocnID: '12309',
            sexChgMdOfceTelNo: 'Details not available',
            sexChgMdDeptID: '1',
            sexChgMdDeptDesc: 'Department for Work and Pensions',
            grFlag: '0',
          },
        ],
        deathDateGroup: [
          {
            deathDate: '23-Aug-2017',
            deathDateStartDt: '23-Aug-2017',
            deathDateChgUserID: '111111',
            deathDateChgTermID: '51.108.30.24',
            deathDateGrpStartDtApplDt: '23-Aug-2017',
            deathDateGrpChgMdBenSysID: '590',
            deathDateGrpChgMdBenSysDesc: 'SEF',
            deathDateGrpChgMdOfceLocnID: '12309',
            deathDateGrpChgMdOfceTelNo: 'Details not available',
            deathDateGrpChgMdDeptID: '1',
            deathDateGrpChgMdDeptDesc: 'Department for Work and Pensions',
            deathDateVerificationFlagID: '3',
            deathDateVerificationFlagDesc: 'Verified to Level 3',
          },
        ],
        natSensGroup: [
          {
            natSensRecTypeID: '1',
            natSensRecTypeDesc: 'Unrestricted Access',
            natSensChgUserID: '111111',
            natSensChgTermID: '51.108.30.24',
            natSensRecStartDt: '31-Jul-2017',
            natSensRecStartDtApplDt: '31-Jul-2017',
            natSensRecChgMdBenSysID: '590',
            natSensRecChgMdBenSysDesc: 'SEF',
            natSensRecChgMdOfceLocnID: '12309',
            natSensRecChgMdOfceTelNo: 'Details not available',
            natSensRecChgMdDeptID: '1',
            natSensRecChgMdDeptDesc: 'Department for Work and Pensions',
          },
        ],
        natTpGroup: [
          {
            natTypeID: '0',
            natTypeDesc: 'Not known',
          },
        ],
        otherDataGroup: [
          {
            nifuInterestID: '1',
            nifuInterestDesc: 'NIFU Interest',
            indInterest: 'Y',
          },
        ],
      },
    ],
  },
});
