function cis014(person) {
  const { services } = person.CIS; // services is a string ie "85,675"

  const list = services ? services.split(',') : [];
  return {
    getFullAwardHistoryResponse: {
      personRoleServiceAssmtMap: list.map((dwpServiceKeyID) => ({
        dwpServiceKeyID,
        claimStartDate: '01-Jan-2020',
        personRoleServiceAward: [
          {
            startDate: 25,
            endDate: null,
            awardStartDate: '13-Dec-2020',
            awardEndDate: null,
          },
        ],
      })),
    },
  };
}

module.exports = cis014;
