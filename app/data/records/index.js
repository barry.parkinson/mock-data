/* eslint-disable global-require */
const data = [
  require('./RJ428577B.json'),
  require('./AE767817B.json'),
  require('./AE767806A.json'),
  require('./PT568951D.json'),
  require('./MW200002A.json'),
  require('./GN129520A.json'),
  require('./MW000137A.json'),
  require('./AE767822C.json'),
  require('./PT568952A.json'),
];

module.exports = data;
