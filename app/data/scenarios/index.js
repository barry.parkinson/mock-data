/* eslint-disable global-require */
const scenarios = {
  'match-too-many-results': require('./match-too-many-results'),
};

module.exports = scenarios;
