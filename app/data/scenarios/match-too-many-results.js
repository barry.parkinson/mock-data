// for TIDV, return too many results from FINDr error

module.exports = (req, res) => {
  // logger ?
  res.status(422);
  res.send({
    statusCode: 422,
    status: 'UNPROCESSABLE_ENTITY',
    error: '1',
    message: 'Too many results found.',
    loggingIdentifierValue: 'c815805c-5b9d-48e8-b974-563d88eb186c',
  });
  return true; // indicate I have processed this request
};
