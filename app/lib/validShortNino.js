// is the nino valid - short nino without suffix

module.exports = (nino) => nino && nino.match(/^[A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]$/);
