// send response and log info

function sendAndLog(req, res, api, status, response, request, responseToLog) {
  res.status(status);
  res.send(response);
  req.logger.info({
    api,
    status,
    response: responseToLog || response,
    request,
  });
}

module.exports = sendAndLog;
