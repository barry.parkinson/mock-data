// is the full nino valid - with suffix

module.exports = (nino) => nino && nino.match(/^[A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9][A-Z]$/);
