// Stryker disable all : boilerplate code
const Logger = require('shefcon-dev-idt-node-logger');
const packageJson = require('../../package.json');

module.exports = (app) => {
  app.use((req, res, next) => {
    req.logger = Logger({
      name: 'idt-mocks',
      version: packageJson.version,
    }, {
      correlation_id: req.get('correlationId') || req.get('correlation-id'),
      session_id: req.get('sessionId') || req.get('session-id'),
      flattenMessage: true,
    });

    next();
  });
};
