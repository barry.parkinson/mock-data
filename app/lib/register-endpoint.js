// register mock endpoints for documentation

const endpoints = [];

function registerEndpoint({
  id,
  description,
  method,
  url,
  headers,
  headerNotes,
  body,
  bodyNotes,
}) {
  endpoints.push({
    id,
    description,
    method,
    url,
    headers,
    headerNotes,
    body,
    bodyNotes,
  });
}

function findById(id) {
  return endpoints.find((endpoint) => id === endpoint.id);
}

module.exports = {
  registerEndpoint,
  endpoints,
  findById,
};
