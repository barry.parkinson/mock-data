// make a uuid, example d7f7d739-cb9e-4de9-9cba-cc5bd4c75ac0

function encode(text) { // text should be 16 chars
  const raw = Buffer.from(text)
    .toString('hex');
  return `${raw.substring(0, 8)}-${raw.substring(8, 12)}-${raw.substring(12, 16)}-${raw.substring(16, 20)}-${raw.substring(20)}`;
}

function decode(uuid) {
  const text = uuid.replace(/-/ig, '');
  return Buffer.from(text, 'hex')
    .toString();
}

module.exports = {
  encode,
  decode,
};

// function test() {
//   const enc = encode('RJ428577B-Z-12345');
//   const dec = decode(enc);
//   console.log(enc, JSON.stringify(dec));
// }
//
// test();
