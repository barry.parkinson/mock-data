const initialList = require('../data/records');

const personList = [];
const guidToNino = new Map();
const shortNinoToPerson = new Map();
const ninoToPerson = new Map();

function addPerson(person) {
  personList.push(person);
  const { nino, guid } = person;
  const short = nino.substring(0, 8);
  ninoToPerson.set(nino, person);
  shortNinoToPerson.set(short, person);
  guidToNino.set(guid, nino);
}

initialList.forEach((person) => {
  addPerson(person);
});

module.exports = () => ({
  personList,
  ninoToPerson,
  shortNinoToPerson,
  guidToNino,
  addPerson,
});
