# idt-stubs-express-monorepo

A unified mock - a single source of truth for mock data

## Using the mocks

1. Start the service with `npm start`
2. Go to http://localhost:8080/mock-endpoints to see the available APIs

You can see the test data listed on http://localhost:8080/mock-info

## Adding mock data

For example, to add a record for NINO `NA123456B`

1. Go to the `/app/data/records` directory
2. Add a file NA123456B.json containing your data
3. Update `/app/data/records/index.js` to add a line `require('./NA123456B.json'),`
4. Run the unit tests with `npm test`

For more details see [Adding Data](/docs/adding-data.md)

## x-data-import

This folder contains files copied from the existing mock repos

Running the import will combine these with the existing records,
potentially overwriting data in `/app/data/records`

Compare the changes made with git before committing

**Only do this if you know what you are doing**
