const cisData = require('./data/cis-data');
const database = require('./database');

// console.log(JSON.stringify(cisData.AE767801, undefined, '  '));

function getServices(data) {
  if (!data.personRoleServiceAssmtMap) {
    return undefined;
  }
  return data.personRoleServiceAssmtMap.map((r) => r.dwpServiceKeyID).join(',');
}

function getCisContactDetail(contactDetails) {
  return contactDetails.map((row) => `${row.contactTypeID}:${row.detail}`);
}

const keys = Object.keys(cisData);
keys.forEach((key) => {
  const data = cisData[key];
  const services = getServices(data);
  const record = database.getByShortNino(key);

  // eslint-disable-next-line no-console
  // console.log(key, Services, rel.length, record ? 'FOUND' : '');

  if (record) {
    database.add({
      nino: record.nino,
      guid: record.guid || '',
      CIS: {
        services,
        contactDetails: getCisContactDetail(data.contactDetails),
        personRoleRelationships: data.personRoleRelationships,
      },
    });
  } else {
    // eslint-disable-next-line no-console
    console.log('CIS_NOT_FOUND', key);
  }
});
