const moment = require('moment');
const esaData = require('./data/iag-data');
const database = require('./database');

moment.parseTwoDigitYear = (yearString) => parseInt(yearString, 10)
  + (parseInt(yearString, 10) > 4 ? 1900 : 2000);

function convertEsaDate(text) {
  return (text && moment(text, 'DD/MM/YY').format('YYYY-MM-DD')) || '';
}

const keys = Object.keys(esaData);
keys.forEach((key) => {
  const row = esaData[key];
  row.claimDate = convertEsaDate(row.claimDate);
  row.lastPaid = convertEsaDate(row.lastPaid);
  const record = {
    nino: key,
    ESA: row,
  };
  database.add(record);
});

// eslint-disable-next-line no-console
// console.log(esaData);

// NOTE next payment response is very simple
// {
//   "nextPaymentDate": "01/12/22",
//   "nextPaymentAmount": "41500"
// }
