const fs = require('fs');
const database = require('./database');
require('./parse-test-data')();
require('./findr-data');
require('./cis');
require('./iag');
// PIP

const records = database.getRecords();

console.log('TOTAL', records.length); // eslint-disable-line no-console

const saved = [];

// save file for individual record

function saveRecord(record) {
  const { nino } = record;
  saved.push(nino);
  const path = `./app/data/records/${nino}.json`;
  const json = JSON.stringify(record, undefined, '  ');
  fs.writeFileSync(path, json);
}

records.forEach((rec) => saveRecord(rec));

// saveRecord(records.find((r) => r.nino === 'RJ428577B'));
// saveRecord(records.find((r) => r.nino === 'AE767817B'));
// saveRecord(records.find((r) => r.nino === 'AE767806A'));
// saveRecord(records.find((r) => r.nino === 'PT568951D'));
// saveRecord(records.find((r) => r.nino === 'MW200002A'));
// saveRecord(records.find((r) => r.nino === 'GN129520A'));
// saveRecord(records.find((r) => r.nino === 'MW000137A'));
// saveRecord(records.find((r) => r.nino === 'AE767822C'));
// saveRecord(records.find((r) => r.nino === 'PT568952A'));

// save the file /app/data/records/index.js

function saveIndex() {
  const lines = [];
  lines.push('/* eslint-disable global-require */');
  lines.push('const data = [');
  saved.forEach((nino) => {
    lines.push(`  require('./${nino}.json'),`);
  });
  lines.push('];');
  lines.push('');
  lines.push('module.exports = data;');
  lines.push('');

  const path = './app/data/records/index.js';
  fs.writeFileSync(path, lines.join('\n'));
}

saveIndex();
