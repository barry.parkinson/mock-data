const initialList = require('../app/data/records');
// records to be imported

const database = [...initialList];
const ninoToIndex = new Map();
const shortNinoToIndex = new Map();

database.forEach((row, index) => {
  const { nino } = row;
  ninoToIndex.set(nino, index);
  shortNinoToIndex.set(nino.substring(0, 8), index);
});

const defaults = { // values that must be present
  guid: '',
  contactDetails: [],
  CIS: {},
};

function add(record) {
  const { nino } = record;
  const index = ninoToIndex.get(nino);
  if (index >= 0) {
    const current = database[index];
    database[index] = { ...defaults, ...current, ...record };
  } else {
    ninoToIndex.set(nino, database.length);
    shortNinoToIndex.set(nino.substring(0, 8), database.length);
    database.push({ ...defaults, ...record });
  }
}

function getByShortNino(short) {
  const index = shortNinoToIndex.get(short);
  return index >= 0 ? database[index] : undefined;
}

function getByNino(nino) {
  const index = ninoToIndex.get(nino);
  return index >= 0 ? database[index] : undefined;
}

function adjust(row) { // make sure the JSON object has the fields in the right order
  const {
    nino,
    guid,
    comments,
    dateOfBirth,
    gender,
    title,
    forename,
    surname,
    postcode,
    contactDetails,
    aliveCheck,
    idAtRiskCheck,
    CIS,
    PIP,
    ESA,
    scenarios,
  } = row;
  return {
    nino,
    guid,
    comments,
    dateOfBirth,
    gender,
    title,
    forename,
    surname,
    postcode,
    contactDetails,
    aliveCheck,
    idAtRiskCheck,
    ESA,
    CIS,
    PIP,
    scenarios,
  };
}

function getRecords() {
  database.forEach((row, index) => {
    database[index] = adjust(row);
  });
  return database;
}

module.exports = {
  add,
  getRecords,
  getByNino,
  getByShortNino,
};
