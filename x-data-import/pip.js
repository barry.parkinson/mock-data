const moment = require('moment');
const pipData = require('./data/pip-data');
const parseData = require('../app/lib/parse-data');

const { ninoToPerson } = parseData();

function getPipComponents(raw) {
  try {
    const mapped = raw.DeterminationDelivery.DeterminationExecution[0].DeterminationDecision.map(
      (outcome) => outcome.OutcomeID,
    );
    const filtered = mapped.filter((code) => !!code); // remove ['', '']
    return filtered.length ? filtered : undefined;
  } catch (d) {
    return undefined;
  }
}

function convertPipDate(text) {
  return (text && moment(text, 'YYYYMMDD').format('YYYY-MM-DD')) || '';
}

function pip(nino) {
  const raw = pipData[nino];
  // eslint-disable-next-line no-console
  // console.log('PIP DATA', JSON.stringify(raw, undefined, '  '));
  const pi = raw.PaymentInstrument && raw.PaymentInstrument[0];
  const mod = {
    BankAccountNumber: raw.BankAccountNumber,
    BankSortCode: raw.BankSortCode,
    PayDayDecode: raw.PayDayDecode,
    lastPaymentAmount: pi && pi.Amount,
    lastPaymentDate: pi && convertPipDate(pi.FinancialInstruction.InstructionLineItem[0].DueDate),
    lastPaymentEffectiveDate: pi && convertPipDate(pi.EffectiveDate),
    PipComponents: getPipComponents(raw),
  };
  const person = ninoToPerson.get(nino);
  // eslint-disable-next-line no-console
  console.log('PIP >>', person ? nino : `${nino} NOT IN CSV`, mod);
}

// pip('AE767820A'); // all KBV

const keys = Object.keys(pipData);
keys.forEach((key) => pip(key));
