const fs = require('fs');
const { parse } = require('csv-parse/sync'); // eslint-disable-line import/no-unresolved
const database = require('./database');
const deceased = require('./data/not-alive');
const atRisk = require('./data/id-at-risk');

// moment.parseTwoDigitYear = (yearString) => parseInt(yearString, 10)
//   + (parseInt(yearString, 10) > 4 ? 1900 : 2000);

// function getDate(text) {
//   return (text && moment(text, 'DD-MMM-YY').format('YYYY-MM-DD')) || undefined;
// }

module.exports = () => {
  const inputData = fs.readFileSync('./x-data-import/data/test-data.csv', 'utf8');

  const records = parse(inputData, {
    columns: true,
    trim: true,
  });

  records.forEach((record) => {
    const nino = record.NINO;
    const short = nino.substring(0, 8);
    database.add({
      nino,
      guid: record.GUID,
      aliveCheck: deceased.has(short) ? 'fail' : undefined,
      idAtRiskCheck: atRisk.has(short) ? 'fail' : undefined,
    });
  });
};
