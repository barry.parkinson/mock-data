// id at risk API 0038

const atRisk = new Set([
  'SX171501',
  'PT568951', // TIDV - Deceased and at risk
  'WX884548', // OIDV E2E Test
  'PT568952', // TIDV E2E TEst
  'RJ428577', // Kyle Scoby
  'WX779834', // AIDA E2E TEST
  'AC874513', // AIDA - Deceased and at risk
  'AE767555', // AIDA E2E TEST
  'AE797887', // AIDA - Deceased and at risk
]);

module.exports = atRisk;
