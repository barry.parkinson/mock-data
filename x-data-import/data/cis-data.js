const invalidNinoRecord = (partnerRecord) => {
  const record = partnerRecord;
  delete record[0].nino;
  return record;
};

/**
 *  Relationships data mock.
 *
 * @param {Array} relationshipDetails Array of relationship details. Format is -
 * [
 *  [forename, surname, birthDate, relationshipTypeId, serviceKey], // for the children
 *  [forename, surname, birthDate, relationshipTypeId, serviceKey, nino] // for the partners
 * ].
 * @param {string} sourceFlag Optional parameter to set sourceFlag=0 for child-parent relationship.
 * @returns {Array} Array of relationship data objects.
 */
const relationshipData = (relationshipDetails, sourceFlag) => {
  const response = [];
  relationshipDetails.forEach((relation) => {
    response.push({
      personName: [
        {
          forenames: relation[0],
          surname: relation[1],
        },
      ],
      birthDate: relation[2],
      roleRelationshipTypeID: relation[3],
      relDWPServiceKey: relation[4],
      sourceFlag: sourceFlag || '1',
      nino: relation[5] || '', // if nino not present in data that means data is for a child
      ninoSuffix: 'A',
    });
  });
  return response;
};

const generateContactObject = (contactDetails) => {
  const response = [];
  contactDetails.forEach((contact) => {
    response.push({
      contactTypeID: contact[0],
      detail: contact[1],
    });
  });
  return response;
};

const CIS_MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const cisDateString = (awardDate) => {
  let result = null;
  if (awardDate) {
    const day = awardDate.getDate();
    const month = CIS_MONTHS[awardDate.getMonth()];
    const year = awardDate.getFullYear();
    result = `${day}-${month}-${year}`;
  }
  return result;
};

const generatePersonRoleServiceAssmtMap = (serviceKeys) => {
  const response = [];
  for (let i = 0; i < serviceKeys.length; ++i) {
    const service = serviceKeys[i];
    const item = {
      dwpServiceKeyID: service.key,
      claimStartDate: cisDateString(service.claimStartDate || new Date('2020-12-17')),
    };
    if (service.awards) {
      item.personRoleServiceAward = [];
      for (let j = 0; j < service.awards.length; ++j) {
        const award = service.awards[j];
        let startDate = null;
        if (award.startDate) {
          startDate = new Date();
          startDate = new Date(startDate.setMonth(startDate.getMonth() - award.startDate));
        }
        award.awardStartDate = cisDateString(startDate);
        let endDate = null;
        if (award.endDate) {
          endDate = new Date();
          endDate = new Date(endDate.setMonth(endDate.getMonth() - award.endDate));
        }
        award.awardEndDate = cisDateString(endDate);
        item.personRoleServiceAward.push(award);
      }
    }
    response.push(item);
  }
  return response;
};

module.exports = {
  AE767814: {
    contactDetails: generateContactObject([ // data for API 008
      ['3', '07565692111'], // '3' tells us the type of contact. In this case mobile number. this is followed by the number
      // add another array here for another contact detail
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([ // data for API 014
      {
        key: '85', // citizen receives benefit with code 85
        claimStartDate: new Date('2020-12-17'), // used for ESA ATE Activity History Check - pre-2021 is a pass
        awards: [
          {
            startDate: 25, // User was entitled to benefit as of 25 days ago
            endDate: null, // still entitled to it
          },
        ],
      }, // add another object here for another benefit
    ]),
    personRoleRelationships: relationshipData([ // API 009
      ['Singh', 'Shah', '01-Feb-2009', '3', '85'], // they have a child named Singh Shah. They were born on 01/02/2009. relationship type = daughter. Service key= 85. No NINO given => child
    ], '0'), // change to the child record sourceFlag as per NGCC request
    ...relationshipData([
      ['MIKE SIGH', 'TIKE', '23-Mar-1990', '2', '10', 'MW000199'],
    ], '0'),
    // add another relationshipData(...) here for more relationships
  },
  AE767808: {
    contactDetails: generateContactObject([
      ['3', '07900000001'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '1',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  AE767811: {
    contactDetails: generateContactObject([
      ['1', '02085746922'],
      ['3', '07961684342'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '6',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Claire', 'Chapman', '01-Feb-2009', '3', '85'],
      ['Claire', 'Chapman', '01-Feb-2009', '3', '6'],
    ]),
  },
  AE767802: {
    contactDetails: generateContactObject([
      ['1', '01914980010'],
      ['1', '01610541231'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['Partner', 'One', '22-Jan-1990', '2', '85', 'MW000351'],
      ], '0'),
    ],
  },
  AE767809: {
    contactDetails: generateContactObject([
      ['3', '07961684342'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '2',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Janet', 'Brown', '03-Feb-2009', '3', '2'],
    ]),
  },
  LB765460: {
    contactDetails: generateContactObject([
      ['1', '00441144960123'],
      ['3', '00447700900123'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        claimStartDate: new Date('2020-12-17'),
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['NORA', 'PORTER', '28-Apr-1991', '2', '85', 'MW000683'],
      ['LUCY', 'Porter', '01-Apr-2010', '3', '85'],
      ['Daniel Middlename', 'Porter', '01-Apr-2010', '3', '85'],
    ]),
  },
  AE767810: {
    contactDetails: generateContactObject([
      ['1', '01410654223'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Rebecca', 'Smith', '01-Feb-2009', '3', '85'],
    ]),
  },
  AE767812: {
    contactDetails: generateContactObject([
      ['3', '07898712321'],
      ['3', '07943803211'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  AE767813: {
    contactDetails: generateContactObject([
      ['1', '07565692897'],
      ['3', '07565692897'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '1',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Singh', 'Shah', '01-Feb-2009', '3', '1'],
    ]),
  },
  AE767801: {
    contactDetails: generateContactObject([
      ['1', '01914980010'],
      ['1', '01134960010'],
      ['1', '01124960010'],
      ['3', '07700900012'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['CANTA', 'BANTA', '21-Jun-1970', '2', '70', 'MW000662'],
      ['SANTA', 'BANTA', '29-Feb-2000', '3', '70'],
      ['SPARKLE', 'XMAS', '29-Feb-2000', '3', '70'],
    ]),
  },
  PA333485: {
    contactDetails: generateContactObject([
      ['1', '07544725740'],
      ['1', '001161321123'],
      ['3', '07544725740'],
      ['3', '07432288264'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['CANTA', 'BANTA', '30-May-1987', '2', '85', 'MW000358'],
      ['Michael', 'Jackson', '09-Apr-2009', '3', '85'],
    ]),
  },
  AE790802: {
    contactDetails: generateContactObject([
      ['1', '0167112233'],
      ['1', '003534567902121'],
      ['1', '0011190112432'],
      ['3', '07700900012'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['Lorelai Adorabelle', 'Gilligan', '28-Nov-2011', '3', '85'],
        ['Jermey', 'Edwards', '01-Jul-2019', '3', '85'],
      ]),
      ...relationshipData([
        ['Tristan', 'Gilligan', '06-Apr-2007', '3', '85'],
      ], '0'),
    ],
  },
  PA333333: {
    contactDetails: generateContactObject([
      ['1', '07544725740'],
      ['3', '07432288264'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  AE767804: {
    contactDetails: generateContactObject([
      ['1', '01610900123'],
      ['3', '07700900012'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Callum David', 'Thompson', '12-Dec-2008', '3', '675'],
    ]),
  },
  AE767805: {
    contactDetails: generateContactObject([
      ['1', '01410871221'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['CANTA', 'BANTA', '22-Jan-1990', '2', '675', 'MW000351'],
    ], '0'),
  },
  AC974514: {
    contactDetails: generateContactObject([
      ['1', '01204 576830'],
      ['1', '07544725740'],
      ['3', '07432109503'],
      ['3', '079504 56106'],
      ['3', '07432288264'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  AE767806: {
    contactDetails: generateContactObject([
      ['1', '01610900123'],
      ['1', '01189884074'],
      ['1', '07429590060'],
      ['1', '07950456106'],
      ['3', '07455150801'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '1',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '2',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '6',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  AE767807: {
    contactDetails: generateContactObject([
      ['1', '01610900123'],
      ['1', '07710079304'],
      ['3', '07710079304'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '2',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '6',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  MW000678: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '31',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Three', 'Child', '26-Oct-2008', '3', '10'],
    ]),
  },
  MW000679: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '31',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  MW000680: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '31',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  MW000681: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '1',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  MW000682: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '1',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  MW000683: {
    contactDetails: generateContactObject([
      ['1', '01134960010'],
      ['1', '01144960123'],
      ['3', '07432109503'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '31',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['MIKE PIKE', 'TIKE', '04-Jun-1955', '2', '85', 'MW000684'],
        ['Lucy Liu', 'PORTER', '01-Apr-2010', '3', '85'],
        ['DANIEL MIDDLENAME', 'PORTER', '10-Jun-2012', '3', '85'],
      ]),
      ...relationshipData([
        ['MIKE PIKE', 'TIKE', '04-Jun-1955', '2', '10', 'MW000684'],
      ], '0'),
    ],
  },
  MW000684: {
    contactDetails: generateContactObject([
      ['1', '00441144960123'],
      ['3', '00447700900123'],
      ['1', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '10',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['NORA', 'PORTER', '01-Jan-1978', '2', '10', 'MW000683'],
      ['LUCY', 'PORTER', '01-Apr-2010', '3', '85'],
      ['DANIEL MIDDLENAME', 'PORTER', '01-Apr-2010', '3', '85'],
    ]),
  },
  MW000685: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
    ]),
    personRoleRelationships: relationshipData([
      ['NORA', 'PORTER', '01-Jan-1978', '3', '85', 'MW000683'],
    ], '0'),
  },
  MW000686: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
    ]),
    personRoleRelationships: relationshipData([
      ['NORA', 'PORTER', '01-Jan-1978', '3', '85', 'MW000683'],
    ], '0'),
  },
  MW000687: {
    contactDetails: generateContactObject([
      ['1', '01154960099'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '1',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['VEGAN', 'SAUSAGE', '10-Oct-1970', '3', '1'],
    ]),
  },
  MW000688: {
    contactDetails: generateContactObject([
      ['1', '01514960090'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '1',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: invalidNinoRecord(relationshipData([
      ['VEGAN', 'BURGER', '11-Nov-1940', '2', '1', ''],
    ])),
  },
  MW000689: {
    contactDetails: generateContactObject([
      ['1', '9876534567'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '670',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['ALICIA', 'WHITE', '03-Jun-2005', '17', '670'], // step child
      ['HENRY', 'WHITE', '11-Nov-1980', '2', '670', 'MW000697'],
    ]),
  },
  MW000690: {
    contactDetails: generateContactObject([
      ['1', '01914980050'],
      ['3', '07700900015'],
      ['15', '01134960010'],
      ['16', '01154960050'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '104',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['LIZ', 'HANLEY', '01-Jan-1980', '2', '85', 'MW000691'],
      ['LIN', 'HARPER', '05-Aug-2001', '3', '104'],
      ['NIEL MIDDLENAME', 'HARPER', '04-Jul-2003', '3', '85'],
      ['NIKY', 'HANLEY', '03-Jun-2000', '17', '104'], // step child
      ['LIYA', 'HARPER', '02-Apr-2007', '3', '85'],
      ['LIMA', 'HARPER', '01-Apr-2010', '3', '85'],
    ]),
  },
  MW200001: {
    contactDetails: generateContactObject([
      ['1', '01144960123'],
      ['3', '07700900123'],
      ['15', '07700900115'],
      ['16', '01144960116'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '10',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  MW200002: {
    contactDetails: generateContactObject([
      ['1', '01144960123'],
      ['3', '00447700900123'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '10',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  MW000747: {
    contactDetails: generateContactObject([
      ['3', '00447700900123'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '1',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '9',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  MW000748: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '670',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  MW000749: {
    contactDetails: generateContactObject([
      ['1', '07544725740'],
      ['1', '07950456106'],
      ['3', '01134960010'],
      ['3', '01914980010'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '31',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['MIKE PIKE', 'TIKE', '14-Jul-1979', '2', '85', 'MW000990'],
      ['JACKI NORA', 'HARDY', '02-May-2007', '3', '85'],
    ]),
  },
  MW000672: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '10',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  MW000657: {
    contactDetails: generateContactObject([
      ['1', '01144960123'],
      ['3', '07700900123'],
      ['15', '07700900115'],
      ['16', '01144960116'],
    ]),
  },
  MW000923: {
    contactDetails: generateContactObject([
      ['1', '07700900010'],
      ['3', '07700900010'],
      ['15', '07700900010'],
      ['16', '07700900010'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['GEORGE IAN', 'TRIBIANI', '30-Jan-2001', '3', '85'],
    ]),
  },
  MW000868: {
    contactDetails: generateContactObject([
      ['1', '01914980000'],
      ['3', '07700900011'],
      ['15', '01134960000'],
      ['16', '01134960000'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Unanswered Question', 'User', '12-Dec-1960', '1', '675', 'MW000700'],
    ]),
  },
  MW000925: {
    contactDetails: generateContactObject([
      ['1', '01134960000'],
      ['3', '07700900011'],
      ['15', '01134960000'],
      ['16', '01134960000'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['SANTA', 'SIKKA', '10-Apr-1971', '2', '85', 'MW000665'],
      ['PINTA', 'MIKKA', '01-Apr-1975', '2', '85', 'MW000664'],
      ['JINTA', 'ROTA', '25-Apr-1973', '2', '85', 'MW000666'],
      ['BANTA', 'SIKKA', '31-Dec-1999', '3', '85'],
      ['STEP', 'SIKKA', '01-Apr-2002', '17', '85'],
    ]),
  },
  MW000930: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: invalidNinoRecord(relationshipData([
      ['RIYA', 'SINGH', '01-Nov-1990', '2', '70', ''],
    ])),
  },
  MW000866: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '47',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '10',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Mild', 'Chilli', '01-Jan-2000', '3', '47'],
    ]),
  },
  MW000932: {
    contactDetails: generateContactObject([
      ['1', '07555467832'],
      ['3', '07121111445'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '10',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['ONION', 'SOUP', '01-Jan-1950', '2', '10', 'MW000933'],
      ['Child', 'Test', '01-Jan-2002', '3', '10'],
    ]),
  },
  MW000884: {
    contactDetails: generateContactObject([
      ['1', '01914980010'],
      ['3', '07700900012'],
      ['15', '01134960010'],
      ['16', '01124960010'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '10',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Happy New', 'Year', '21-Jun-1970', '2', '10', 'MW000662'],
      ['SANTA BANTA', 'XMAS', '29-Feb-2000', '3', '10'],
      ['SPARKLE', 'XMAS', '29-Feb-2000', '3', '10'],
    ]),
  },
  MW000935: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '10',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Nice', 'Family', '20-May-2005', '3', '10'],
    ]),
  },
  MW000888: {
    contactDetails: generateContactObject([
      ['1', '01914980020'],
      ['3', '07700900013'],
      ['15', '01134960020'],
      ['16', '07700900013'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Merry', 'Joice', '05-Jan-1970', '2', '70', 'MW000600'],
    ]),
  },
  MW000937: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['HARRY', 'SMITH', '29-Feb-2000', '3', '85'],
    ]),
  },
  MW000841: {
    contactDetails: generateContactObject([
      ['1', '01234987853'],
      ['3', '07777999999'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  MW000691: {
    contactDetails: generateContactObject([
      ['1', '01914980050'],
      ['3', '07700900015'],
      ['15', '01134960010'],
      ['16', '01154960050'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      }, {
        key: '10',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['TOM', 'HARPER', '20-Apr-1976', '2', '10', 'MW000690'],
      ['LIN', 'HARPER', '05-Aug-2001', '3', '10'],
      ['NIEL MIDDLENAME', 'HARPER', '04-Jul-2003', '3', '675'],
      ['NIKY', 'HANLEY', '03-Jun-2005', '17', '10'], // step child
    ]),
  },
  MW000845: {
    contactDetails: generateContactObject([
      ['1', '01914980060'],
      ['3', '07700900016'],
      ['15', '07700900016'],
      ['16', '07700900016'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  MW000846: {
    contactDetails: [],
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  AE767815: {
    contactDetails: generateContactObject([
      ['1', '01204576830'],
      ['1', '07795433811'],
      ['1', '07943803211'],
      ['1', '07950456106'],
      ['1', '00447432108501'],
      ['3', '0019765121112'],
      ['3', '00447432108501'],
      ['3', '003534567902333'],
      ['3', '07795433811'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['PARTNER', 'ONE', '21-Dec-1984', '2', '85', 'MW000335'],
      ], '0'), // change to the partner record sourceFlag as per NGCC email
      ...relationshipData([
        ['PARTNER', 'TWO', '22-Jun-1989', '2', '85', 'MW000336'],
        ['Jenifer', 'Jones', '23-Jun-2007', '3', '85'],
        ['Paul Jones', 'Jones', '12-Sep-2009', '3', '85'],
      ]),
    ],
  },
  AE767817: {
    contactDetails: generateContactObject([
      ['1', '02037812345'],
      ['3', '08003289393'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  AE767818: {
    contactDetails: generateContactObject([
      ['1', '02081114129'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '2',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  AE767819: {
    contactDetails: generateContactObject([
      ['1', '02071882321'],
      ['3', '07544725740'],
      ['1', '07544725740'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  AE767820: {
    contactDetails: generateContactObject([
      ['3', '07553210559'],
      ['1', '07553210559'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  AE767821: {
    contactDetails: generateContactObject([
      ['3', '07753210550'],
      ['1', '07753210550'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  AE767822: {
    contactDetails: generateContactObject([
      ['1', '02071882329'],
      ['1', '08003289393'],
      ['3', '08003289393'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  AE767823: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Jenifer', 'Jones', '23-Jun-2007', '3', '85'],
      ['Paul', ' Jones', '12-Sep-2009', '3', '85'],
    ]),
  },
  AE767824: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['Jenifer', 'Jones', '23-Jun-2007', '3', '85'],
      ], '0'), // change eldest child DOD to record sourceFlag 0 as per NGCC data creation email
      ...relationshipData([
        ['Paul', ' Jones', '12-Sep-2009', '3', '85'],
      ]),
    ],
  },
  AE767825: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['PARTNER', 'ONE', '21-Dec-1984', '2', '85', 'MW000335'],
      ], '0'), // change to the partner record sourceFlag as per NGCC email
      ...relationshipData([
        ['PARTNER', 'TWO', '22-Jun-1989', '2', '85', 'MW000336'],
      ]),
    ],
  },
  AE767826: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['PARTNER', 'ONE', '22-Jun-1989', '2', '85', 'MW000336'],
      ]),
    ],
  },
  AE767828: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '80',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      }, {
        key: '8',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  AE211099: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Jenifer', 'Jones', '23-Jun-2007', '3', '675'],
      ['Paul', ' Jones', '12-Sep-2009', '3', '675'],
    ]),
  },
  AE767827: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['PARTNER', 'ONE', '21-Dec-1984', '2', '85', 'MW000335'],
      ], '0'), // change to the partner record sourceFlag as per NGCC email
      ...relationshipData([
        ['PARTNER', 'TWO', '22-Jun-1989', '2', '85', 'MW000336'],
      ]),
    ],
  },
  GN129520: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['PARTNER', 'ONE', '21-Dec-1984', '2', '675', 'MW000335'],
      ], '0'), // change to the partner record sourceFlag as per NGCC email
      ...relationshipData([
        ['PARTNER', 'TWO', '22-Jun-1989', '2', '675', 'MW000336'],
      ]),
    ],
  },
  HP127025: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07432108501'],
      ['1', '07950456106'],
      ['3', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '46',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '6',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '31',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  HP129521: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['PARTNER', 'ONE', '22-Jun-1989', '2', '675', 'MW000336'],
      ]),
    ],
  },
  HP126924: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  MW000138: {
    contactDetails: generateContactObject([
      ['3', '07455150801'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '47',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  AE213099: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['PARTNER', 'ONE', '21-Dec-1984', '2', '675', 'MW000335'],
      ], '0'), // change to the partner record sourceFlag as per NGCC email
      ...relationshipData([
        ['PARTNER', 'TWO', '22-Jun-1989', '2', '675', 'MW000336'],
      ]),
    ],
  },
  HP126823: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '14',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  MW000135: {
    contactDetails: generateContactObject([
      ['3', '07908123659'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap(['']),
    personRoleRelationships: [],
  },
  MW000139: {
    contactDetails: generateContactObject([
      ['3', '07455150801'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '23',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  HP127126: {
    contactDetails: generateContactObject([
      ['1', '01619516838'],
      ['1', '07795433811'],
      ['1', '07984364100'],
      ['3', '07432108501'],
      ['3', '07950456106'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '34',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  MW000140: {
    contactDetails: generateContactObject([
      ['3', '07455150801'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '4',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  MW000141: {
    contactDetails: generateContactObject([
      ['3', '07455150801'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '2',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  MW000142: {
    contactDetails: generateContactObject([
      ['3', '07455150801'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '49',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  MW000143: {
    contactDetails: generateContactObject([
      ['3', '07455150801'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '104',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  MW000144: {
    contactDetails: generateContactObject([
      ['3', '07455150801'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '670',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [],
  },
  MW000155: {
    contactDetails: generateContactObject([
      ['1', '01204576830'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['PARTNER', 'ONE', '21-Dec-1984', '2', '675', 'MW000335'],
    ], '0'),
  },
  MW000187: {
    contactDetails: generateContactObject([
      ['1', '01204576830'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Jenifer', 'Jones', '21-Dec-1984', '3', '675'],
    ], '0'), // change eldest child DOD to record sourceFlag 0 as per NGCC data creation email
  },
  AE797558: {
    contactDetails: generateContactObject([
      ['1'],
      ['3', '01204576830'],
    ]),
  },

  RJ428577: {
    contactDetails: generateContactObject([
      ['1', '07981230431'],
      ['1', '01610541231'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Claire', 'Chapman', '23-Mar-1990', '2', '10', 'MW000199'],
      ['Ella', 'Chapman', '01-Feb-2007', '3', '85'],
      ['Charlie', 'Chapman', '01-Feb-2009', '3', '6'],
    ]),
  },
  PT568951: {
    contactDetails: generateContactObject([
      ['1', '01914980010'],
      ['1', '01134960010'],
      ['1', '01124960010'],
      ['3', '07700900012'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['CANTA', 'BANTA', '21-Jun-1970', '2', '70', 'MW000662'],
      ['SANTA', 'BANTA', '29-Feb-2000', '3', '70'],
      ['SPARKLE', 'XMAS', '29-Feb-2000', '3', '70'],
    ]),
  },
  PT568952: {
    contactDetails: generateContactObject([
      ['1', '01914980010'],
      ['1', '01134960010'],
      ['1', '01124960010'],
      ['3', '07700900012'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['CANTA', 'BANTA', '21-Jun-1970', '2', '70', 'MW000662'],
      ['SANTA', 'BANTA', '29-Feb-2000', '3', '70'],
      ['SPARKLE', 'XMAS', '29-Feb-2000', '3', '70'],
    ]),
  },
  AE767556: {
    contactDetails: generateContactObject([
      ['1', '01914980010'],
      ['1', '01134960010'],
      ['1', '01124960010'],
      ['3', '07700900012'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['CANTA', 'BANTA', '21-Jun-1970', '2', '70', 'MW000662'],
      ['SANTA', 'BANTA', '29-Feb-2000', '3', '70'],
      ['SPARKLE', 'XMAS', '29-Feb-2000', '3', '70'],
    ]),
  },
  AE797557: {
    contactDetails: generateContactObject([
      ['1', '01914980010'],
      ['1', '01134960010'],
      ['1', '01124960010'],
      ['3', '07700900012'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['CANTA', 'BANTA', '21-Jun-1970', '2', '70', 'MW000662'],
      ['SANTA', 'BANTA', '29-Feb-2000', '3', '70'],
      ['SPARKLE', 'XMAS', '29-Feb-2000', '3', '70'],
    ]),
  },
  AE767886: {
    contactDetails: generateContactObject([
      ['1', '01914980010'],
      ['1', '01134960010'],
      ['1', '01124960010'],
      ['3', '07700900012'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '675',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '70',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['CANTA', 'BANTA', '21-Jun-1970', '2', '70', 'MW000662'],
      ['SANTA', 'BANTA', '29-Feb-2000', '3', '70'],
      ['SPARKLE', 'XMAS', '29-Feb-2000', '3', '70'],
    ]),
  },
  WX779834: {
    contactDetails: generateContactObject([
      ['1', '0161239890'],
      ['3', '07479467190'],
      ['3', '07979439769'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        claimStartDate: new Date('2020-12-17'),
        awards: [
          {
            startDate: 40,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['PARTNER', 'ONE', '21-Dec-1984', '2', '85', 'MW000444'],
      ], '0'),
      ...relationshipData([
        ['PARTNER', 'TWO', '22-Jun-1989', '2', '85', 'MW000445'],
      ]),
    ],
  },
  NW880008: {
    contactDetails: generateContactObject([
      ['1', '07765990321'],
      ['3', '07976542008'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        claimStartDate: new Date('2019-06-22'),
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
  },
  AC874513: {
    contactDetails: generateContactObject([
      ['1', '0191833290'],
      ['1', '01132567991'],
      ['1', '01132844285'],
      ['3', '07701344544'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '670',
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
      {
        key: '85',
        claimStartDate: new Date('2019-09-20'),
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Velma', 'Verma', '21-Jun-1970', '2', '70', 'MW000835'],
      ['Victor', 'Verma', '29-Feb-2000', '3', '70'],
      ['Vincent', 'Verma', '10-Jan-2005', '3', '70'],
    ]),
  },
  CT129093: {
    contactDetails: generateContactObject([
      ['1', '01619516892'],
      ['3', '07432108945'],
      ['3', '07950456111'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        claimStartDate: new Date('2022-09-20'),
        awards: [
          {
            startDate: 10,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: [
      ...relationshipData([
        ['John', 'Holding', '21-Aug-1984', '2', '85', 'MW000387'],
      ], '0'),
      ...relationshipData([
        ['Jimmy', 'Cricket', '22-Jun-1990', '2', '85', 'MW000422'],
      ]),
    ],
  },
  AE767555: {
    contactDetails: generateContactObject([
      ['1', '01914676789'],
      ['3', '07700978653'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        claimStartDate: new Date('2022-09-20'),
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['Rose', 'Mary', '21-May-2010', '3', '70'],
    ]),
  },
  AE797887: {
    contactDetails: generateContactObject([
      ['1', '01818118003'],
      ['3', '07927665823'],
    ]),
    personRoleServiceAssmtMap: generatePersonRoleServiceAssmtMap([
      {
        key: '85',
        claimStartDate: new Date('2022-09-20'),
        awards: [
          {
            startDate: 25,
            endDate: null,
          },
        ],
      },
    ]),
    personRoleRelationships: relationshipData([
      ['James', 'Bailey', '14-Jan-1984', '3', '70'],
    ]),
  },
  HP006001: {
    contactDetails: generateContactObject([
      ['3', '01204576831'],
    ]),
  },
  HP006002: {
    contactDetails: generateContactObject([
      ['3', '01204576831'],
    ]),
  },
  HP006003: {
    contactDetails: generateContactObject([
      ['3', '01204576831'],
    ]),
  },
  HP006004: {
    contactDetails: generateContactObject([
      ['3', '01204576831'],
    ]),
  },
  SX170101: {
    contactDetails: generateContactObject([
      ['3', '01204576831'],
    ]),
  },
  SX172401: {
    contactDetails: generateContactObject([
      ['3', '01204576831'],
    ]),
  },
  SX173001: {
    contactDetails: generateContactObject([
      ['3', '01204576831'],
    ]),
  },
  SX710101: {
    contactDetails: generateContactObject([
      ['3', '01204576831'],
    ]),
  },
  WB116666: {
    contactDetails: generateContactObject([
      ['3', '01204576831'],
    ]),
  },
  SX172001: {
    contactDetails: generateContactObject([
      ['3', '01204576831'],
    ]),
  },
  SX171501: {
    contactDetails: generateContactObject([
      ['3', '01204576831'],
    ]),
  },
};
