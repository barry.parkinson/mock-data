// used by alive-check - note these are the first 8 chars of the nino without the suffix

const deceased = new Set([
  'MW200002',
  'MW000352',
  'MW006789',
  'AE767805',
  'PT568951', // Alive check and id at risk fail
]);

module.exports = deceased;
