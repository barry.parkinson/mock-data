module.exports = {
  AE767813A: {
    BankAccountNumber: '12342030',
    BankSortCode: '404784',
    StartDate: '20210423',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10004',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10004',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '20191220',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },
  AE767820A: {
    BankAccountNumber: '12342200',
    BankSortCode: '404784',
    StartDate: '20091220',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1', // benefit is active
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10004',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10004',
        }],
      }],
    },
    PayDayDecode: 'Wednesday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '57771',
      EffectiveDate: '20210120',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20210120' },
        ],
      },
    }],
  },
  AE767821C: {
    BankAccountNumber: '12342211',
    BankSortCode: '404784',
    StartDate: '20091220',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10002',
        }, {
          OutcomeID: '10004',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10002',
        }, {
          OutcomeID: '10004',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '31500',
      EffectiveDate: '20210120',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20210120' },
        ],
      },
    }],
  },
  AE767810C: {
    BankAccountNumber: '12342030',
    BankSortCode: '504784',
    StartDate: '20091220',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10002',
        }, {
          OutcomeID: '10004',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10002',
        }, {
          OutcomeID: '10004',
        }],
      }],
    },
    PayDayDecode: 'Friday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '56151',
      EffectiveDate: '20191220',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191220' },
        ],
      },
    }],
  },
  AE767807B: {
    BankAccountNumber: '12347051',
    BankSortCode: '404784',
    StartDate: '20190701',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10005',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10005',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '21022',
      EffectiveDate: '20191201',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191201' },
        ],
      },
    }],
  },
  AE767806A: {
    BankAccountNumber: '12342003',
    BankSortCode: '404784',
    StartDate: '20190925',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10002',
        }, {
          OutcomeID: '10004',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10002',
        }, {
          OutcomeID: '10004',
        }],
      }],
    },
    PayDayDecode: 'Friday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '56151',
      EffectiveDate: '20191220',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191220' },
        ],
      },
    }],
  },
  AC974514C: {
    BankAccountNumber: '12346070',
    BankSortCode: '400784',
    StartDate: '20191020',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10002',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10002',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Tuesday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '56151',
      EffectiveDate: '20191220',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191220' },
        ],
      },
    }],
  },
  AE767805B: {
    BankAccountNumber: '12347050',
    BankSortCode: '404784',
    StartDate: '20191019',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10002',
        }, {
          OutcomeID: '10005',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10002',
        }, {
          OutcomeID: '10005',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '21022',
      EffectiveDate: '20191219',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191219' },
        ],
      },
    }],
  },
  AE767804B: {
    BankAccountNumber: '12346070',
    BankSortCode: '404784',
    StartDate: '20171218',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10004',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10004',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '21022',
      EffectiveDate: '20210830',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20210830' },
        ],
      },
    }],
  },
  AE767802B: {
    BankAccountNumber: '12347050',
    BankSortCode: '404784',
    StartDate: '20181221',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10005',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10005',
        }],
      }],
    },
    PayDayDecode: 'Tuesday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '21022',
      EffectiveDate: '20191221',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191221' },
        ],
      },
    }],
  },
  AE790802B: {
    BankAccountNumber: '12342030',
    BankSortCode: '404784',
    StartDate: '20111219',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10004',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10004',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '56151',
      EffectiveDate: '20191219',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191219' },
        ],
      },
    }],
  },
  PA333485B: {
    BankAccountNumber: '12347070',
    BankSortCode: '404784',
    StartDate: '20191028',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '23645',
      EffectiveDate: '20191217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191217' },
        ],
      },
    }],
  },
  AE767801B: {
    BankAccountNumber: '12342490',
    BankSortCode: '404784',
    StartDate: '20191028',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '41250',
      EffectiveDate: '20191217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191217' },
        ],
      },
    }],
  },
  MW000651A: {
    BankAccountNumber: '64848995',
    BankSortCode: '979797',
    StartDate: '20181009',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Kebohe Qecukojak',
      Amount: '1189714',
      EffectiveDate: '20201217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181025' },
          { DueDate: '20181025' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
        ],
      },
    }],
  },
  MW000652A: {
    BankAccountNumber: '64848995',
    BankSortCode: '979797',
    StartDate: '20181009',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201217105325',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201217105407',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Kebohe Qecukojak',
      Amount: '1189714',
      EffectiveDate: '20201217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181025' },
          { DueDate: '20181025' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
        ],
      },
    }],
  },
  MW000653A: {
    BankAccountNumber: '64848995',
    BankSortCode: '979797',
    StartDate: '20181009',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201217105325',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201217105407',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Kebohe Qecukojak',
      Amount: '1189714',
      EffectiveDate: '20201217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181025' },
          { DueDate: '20181025' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
        ],
      },
    }],
  },
  MW000654A: {
    BankAccountNumber: '64848995',
    BankSortCode: '979797',
    StartDate: '20181009',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201217105325',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201217105407',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Kebohe Qecukojak',
      Amount: '1189714',
      EffectiveDate: '20201217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181025' },
          { DueDate: '20181025' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
        ],
      },
    }],
  },
  MW000658A: {
    BankAccountNumber: '64848995',
    BankSortCode: '979797',
    StartDate: '20181009',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201217105325',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201217105407',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Kebohe Qecukojak',
      Amount: '1189714',
      EffectiveDate: '20201217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181025' },
          { DueDate: '20181025' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
        ],
      },
    }],
  },
  MW000661A: {
    BankAccountNumber: '64848995',
    BankSortCode: '979797',
    StartDate: '20181009',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201217105325',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201217105407',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Kebohe Qecukojak',
      Amount: '1189714',
      EffectiveDate: '20201217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181025' },
          { DueDate: '20181025' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
        ],
      },
    }],
  },
  MW000683A: {
    BankAccountNumber: '98273846781ABGASHJD6',
    BankSortCode: '',
    StartDate: '20181009',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201217105325',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201217105407',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Kebohe Qecukojak',
      Amount: '1189714',
      EffectiveDate: '20201217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181025' },
          { DueDate: '20181025' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
        ],
      },
    }],
  },
  MW000747A: {
    BankAccountNumber: '64848995',
    BankSortCode: '979797',
    StartDate: '20181009',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201217105325',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201217105407',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Kebohe Qecukojak',
      Amount: '1189714',
      EffectiveDate: '20201217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181025' },
          { DueDate: '20181025' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
        ],
      },
    }],
  },
  MW000748A: {
    BankAccountNumber: '64848995',
    BankSortCode: '979797',
    StartDate: '20181009',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201217105325',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201217105407',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Kebohe Qecukojak',
      Amount: '1189714',
      EffectiveDate: '20201217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181025' },
          { DueDate: '20181025' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
        ],
      },
    }],
  },
  MW000749A: {
    BankAccountNumber: '64848995',
    BankSortCode: '979797',
    StartDate: '20181009',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201217105325',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201217105407',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Thursday',
    PaymentInstrument: [{
      NomineeName: 'Kebohe Qecukojak',
      Amount: '1189714',
      EffectiveDate: '20201217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181025' },
          { DueDate: '20181025' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
        ],
      },
    }],
  },
  MW000884A: {
    BankAccountNumber: '70872490',
    BankSortCode: '404784',
    StartDate: '20181009',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Kebohe Qecukojak',
      Amount: '412.50',
      EffectiveDate: '20191217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181025' },
          { DueDate: '20181025' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
          { DueDate: '20190606' },
          { DueDate: '20190704' },
          { DueDate: '20190801' },
          { DueDate: '20190829' },
          { DueDate: '20190926' },
          { DueDate: '20191024' },
          { DueDate: '20191121' },
          { DueDate: '20191219' },
          { DueDate: '20200116' },
          { DueDate: '20200213' },
          { DueDate: '20200312' },
          { DueDate: '20200409' },
          { DueDate: '20200507' },
          { DueDate: '20200604' },
          { DueDate: '20200702' },
          { DueDate: '20200730' },
          { DueDate: '20200827' },
          { DueDate: '20200924' },
          { DueDate: '20201022' },
          { DueDate: '20201119' },
          { DueDate: '20201217' },
          { DueDate: '20181122' },
          { DueDate: '20181220' },
          { DueDate: '20190117' },
          { DueDate: '20190214' },
          { DueDate: '20190314' },
          { DueDate: '20190411' },
          { DueDate: '20190509' },
        ],
      },
    }],
  },
  MW200002A: {
    BankAccountNumber: '12345670',
    BankSortCode: '020202',
    StartDate: '20191028',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10005',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10005',
        }],
      }],
    },
    PayDayDecode: 'Wednesday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '97584',
      EffectiveDate: '20201028',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20200902' },
          { DueDate: '20200930' },
          { DueDate: '20201028' },
        ],
      },
    }],
  },
  MW200001A: {
    BankAccountNumber: '12345670',
    BankSortCode: '020202',
    StartDate: '20180818',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201026154329',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201026154415',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Vilate Danoxetaj',
      Amount: '1189714',
      EffectiveDate: '20201028',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20181001' },
          { DueDate: '20181029' },
          { DueDate: '20181126' },
          { DueDate: '20181224' },
          { DueDate: '20190121' },
          { DueDate: '20190218' },
          { DueDate: '20190318' },
          { DueDate: '20190415' },
          { DueDate: '20190513' },
          { DueDate: '20190610' },
          { DueDate: '20190708' },
          { DueDate: '20190805' },
          { DueDate: '20190902' },
          { DueDate: '20190930' },
          { DueDate: '20191028' },
          { DueDate: '20191125' },
          { DueDate: '20191223' },
          { DueDate: '20200120' },
          { DueDate: '20200217' },
          { DueDate: '20200316' },
          { DueDate: '20200413' },
          { DueDate: '20200511' },
          { DueDate: '20200608' },
          { DueDate: '20200706' },
          { DueDate: '20200803' },
          { DueDate: '20200831' },
          { DueDate: '20200928' },
          { DueDate: '20201026' },
          { DueDate: '20181001' },
          { DueDate: '20181029' },
          { DueDate: '20181126' },
          { DueDate: '20181224' },
          { DueDate: '20190121' },
          { DueDate: '20190218' },
          { DueDate: '20190318' },
          { DueDate: '20190415' },
          { DueDate: '20190513' },
          { DueDate: '20190610' },
          { DueDate: '20190708' },
          { DueDate: '20190805' },
          { DueDate: '20190902' },
          { DueDate: '20190930' },
          { DueDate: '20191028' },
          { DueDate: '20191125' },
          { DueDate: '20191223' },
          { DueDate: '20200120' },
          { DueDate: '20200217' },
          { DueDate: '20200316' },
          { DueDate: '20200413' },
          { DueDate: '20200511' },
          { DueDate: '20200608' },
          { DueDate: '20200706' },
          { DueDate: '20200803' },
          { DueDate: '20200831' },
          { DueDate: '20200928' },
          { DueDate: '20201026' },
          { DueDate: '20180903' },
          { DueDate: '20180903' },
        ],
      },
    }],
  },
  YT382347D: {
    PayDayDecode: 'Friday',
    BankAccountNumber: '8187272',
    BankSortCode: '404040',
    StartDate: '20191028',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201026154329',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '97584',
      EffectiveDate: '20201028',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20200902' },
          { DueDate: '20200930' },
          { DueDate: '20201028' },
        ],
      },
    }],
  },
  // NGCC new data
  AE211099C: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: 'Tuesday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },

  MW000135A: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: 'Wednesday',
    PaymentInstrument: [{
      NomineeName: '',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: '',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },

  AE213099C: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '876.54',
      EffectiveDate: '20210201',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20210201' },
        ],
      },
    }],
  },
  GN129520A: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '123.45',
      EffectiveDate: '20210429',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20210429' },
        ],
      },
    }],
  },
  HP129521B: {
    BankAccountNumber: '12343456',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },
  HP126722B: {
    BankAccountNumber: '',
    BankSortCode: '404784',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },
  HP126823B: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10004',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10004',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },
  HP126924A: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '20171218',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },
  MW000137A: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: 'Tuesday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },
  MW000139B: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10004',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10004',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },
  HP127025A: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }],
    },
  },

  MW000138A: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '',
        }],
      }],
    },
  },

  MW000140B: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },

  MW000141C: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10003',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10003',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },

  MW000142A: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },

  MW000143B: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10004',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10004',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },

  MW000144B: {
    BankAccountNumber: '',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10004',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '10000',
        }, {
          OutcomeID: '10004',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },

  MW000155B: {
    BankAccountNumber: '12347766',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },
  MW000187B: {
    BankAccountNumber: '12347766',
    BankSortCode: '',
    StartDate: '',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }, {
        DecisionDate: '',
        DeterminationDecision: [{
          OutcomeID: '',
        }, {
          OutcomeID: '',
        }],
      }],
    },
    PayDayDecode: '',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '',
      EffectiveDate: '',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '' },
        ],
      },
    }],
  },

  PT568951D: {
    BankAccountNumber: '12342490',
    BankSortCode: '404784',
    StartDate: '20191028',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '41250',
      EffectiveDate: '20191217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191217' },
        ],
      },
    }],
  },
  PT568952A: {
    BankAccountNumber: '12342490',
    BankSortCode: '404784',
    StartDate: '20191028',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '41250',
      EffectiveDate: '20191217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191217' },
        ],
      },
    }],
  },
  AE767556B: {
    BankAccountNumber: '12342490',
    BankSortCode: '404784',
    StartDate: '20191028',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '41250',
      EffectiveDate: '20191217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191217' },
        ],
      },
    }],
  },
  AE797557B: {
    BankAccountNumber: '12342490',
    BankSortCode: '404784',
    StartDate: '20191028',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '41250',
      EffectiveDate: '20191217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191217' },
        ],
      },
    }],
  },
  AE767886: {
    BankAccountNumber: '12342490',
    BankSortCode: '404784',
    StartDate: '20191028',
    DeterminationDelivery: {
      StatusCode: 'CMP',
      RecordStatusCode: 'RST1',
      DeterminationExecution: [{
        DecisionDate: '20201028171537',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }, {
        DecisionDate: '20201028171645',
        DeterminationDecision: [{
          OutcomeID: '10001',
        }, {
          OutcomeID: '10003',
        }],
      }],
    },
    PayDayDecode: 'Monday',
    PaymentInstrument: [{
      NomineeName: 'Smith Alan',
      Amount: '41250',
      EffectiveDate: '20191217',
      FinancialInstruction: {
        StatusCode: 'ISS',
        InstructionLineItem: [
          { DueDate: '20191217' },
        ],
      },
    }],
  },
  HP006001: {
    PayDayDecode: 'Monday',
  },
  HP006002: {
    PayDayDecode: 'Monday',
  },
  HP006003: {
    PayDayDecode: 'Monday',
  },
  HP006004: {
    PayDayDecode: 'Monday',
  },
  SX170101: {
    PayDayDecode: 'Monday',
  },
  SX172401: {
    PayDayDecode: 'Monday',
  },
  SX173001: {
    PayDayDecode: 'Monday',
  },
  SX710101: {
    PayDayDecode: 'Monday',
  },
  WB116666: {
    PayDayDecode: 'Monday',
  },
  SX172001: {
    PayDayDecode: 'Monday',
  },
  SX171501: {
    PayDayDecode: 'Monday',
  },
};
