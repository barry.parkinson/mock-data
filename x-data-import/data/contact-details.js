// used by findr

const contactDetails = {
  RJ428577B: ['01610001234', 'scoby@gmail.com'],
  AE767818D: ['0171888888234', 'scoby@gmail.com'],
  NW123456B: ['01132212121', 'emmar@gmail.com'],
  // TIDV
  GN129520A: ['0428096805'],
  AE767817B: ['9099014817'],
  MW200002A: ['00919498936244'],
  MW000137A: ['0426793225'],
  AE767822C: ['9099014821', '02071882329', '08003289393'],
  AE797887B: ['00919498936244', '9099014821'],
};

module.exports = contactDetails;
