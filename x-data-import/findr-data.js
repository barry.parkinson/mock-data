const fs = require('fs');
const moment = require('moment');
const { parse } = require('csv-parse/sync'); // eslint-disable-line import/no-unresolved
const database = require('./database');

function mapContact(text) {
  return text ? text.split('|') : [];
}

function adjustDate(text) {
  return (text && moment(text, 'DD-MMM-YYYY')
    .format('YYYY-MM-DD')) || undefined;
}

function upper(text) {
  return text && text.toUpperCase();
}

function go() {
  const inputData = fs.readFileSync('./x-data-import/data/acceptanceTestData.csv', 'utf8');

  const records = parse(inputData, {
    columns: true,
    trim: true,
  });

  records.forEach((record) => {
    const {
      nino,
      nino_suffix, // eslint-disable-line camelcase
      birthDate, // 26-Dec-2000
      contactDetail,
      postCode,
      sex,
      title,
      forenames: forename,
      surname,
      // addressLine1 townCity county
    } = record;
    const dateOfBirth = adjustDate(birthDate);
    const contactDetails = mapContact(contactDetail);
    const postcode = upper(postCode);
    const gender = upper(sex);

    database.add({
      nino: nino + nino_suffix, // eslint-disable-line camelcase
      dateOfBirth,
      gender,
      title: title || undefined,
      forename,
      surname,
      postcode,
      contactDetails,
    });
  });
}

go();
