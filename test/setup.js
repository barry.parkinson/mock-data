// jest setup

jest.mock('shefcon-dev-idt-node-logger', () => {
  // const log = (a) => process.stdout.write(`${JSON.stringify(a)}\n`);
  const log = undefined;
  const error = jest.fn(log);
  const info = jest.fn(log);
  const warn = jest.fn(log);
  return () => ({
    error,
    warn,
    info,
  });
});
