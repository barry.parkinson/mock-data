const validFullNino = require('../../app/lib/validFullNino');
const data = require('../../app/data/records');

const errors = [];

function isDateValid(text) {
  return text && text.match(/^[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$/);
}

function assertDate(text, nino, field) {
  if (text) {
    if (!isDateValid(text)) {
      errors.push(`${nino} ${field} - date format error: ${text}`);
      return;
    }
    const d = new Date(`${text}T00:00:00.000Z`);
    let c = text;
    try {
      c = d.toISOString().substring(0, 10);
    } catch (e) {
      errors.push(`${nino} ${field} - (${d}) date error: ${text}`);
    }
    if (c !== text) {
      errors.push(`${nino} ${field} - (${c}) date error: ${text}`);
    }
  }
}

function assertAmount(text, nino, field) {
  if (text) {
    const i = text.indexOf('.');
    if (i !== text.length - 3) {
      errors.push(`${nino} ${field} - decimal point error: ${text}`);
      return;
    }
    if (Number.isNaN(Number.parseFloat(text))) {
      errors.push(`${nino} ${field} - NaN error: ${text}`);
    }
  }
}

function verifyPerson(person) {
  const { nino, dateOfBirth } = person;
  if (!validFullNino(nino)) {
    errors.push(`${nino} INVALID NINO`);
  }
  assertDate(dateOfBirth, nino, 'dateOfBirth');
  if (!person.CIS) {
    errors.push(`${nino} must have a CIS field ie "CIS": {}`);
  }

  if (person.PIP) {
    // PayDayDecode must be Monday, etc
  }
  if (person.CIS) {
    // ?
  }
  if (person.ESA) {
    const {
      nextPaymentDate, nextPaymentAmount, amount, lastPaid,
    } = person.ESA;
    assertDate(nextPaymentDate, nino, 'ESA.nextPaymentDate');
    assertDate(lastPaid, nino, 'ESA.lastPaid');
    assertAmount(amount, nino, 'ESA.amount');
    assertAmount(nextPaymentAmount, nino, 'ESA.nextPaymentAmount');
    // "bwe": "WED"
    // "accountDetails": "Bank acc No: 12343211 Bank sort code: 40-47-84",
    // "accountDetails": "BSoc acc No: 65432987654502   BSoc Code: 18",
  }
}

function verifyData() {
  data.forEach((person) => verifyPerson(person));
}

describe('verify test data', () => {
  test('all ids are unique', () => {
    const ninos = new Set();
    const guids = new Set();
    data.forEach(({ nino, guid }) => {
      if (ninos.has(nino)) {
        throw new Error(`Nino is not unique: ${nino}`);
      }
      ninos.add(nino);
      if (guid) {
        if (guids.has(guid)) {
          throw new Error(`DWP Guid is not unique: ${guid}`);
        }
        guids.add(guid);
      }
    });
  });

  test('customer data is valid', () => {
    verifyData();
    if (errors.length) {
      console.log(errors); // eslint-disable-line no-console
      expect(errors).toEqual([]);
    }
  });
});
