const request = require('supertest');
const cisApi0038FailResponse = require('../../app/data/responses/cisApi0038FailResponse');
const cisFaultInvalidShortNino = require('../../app/data/responses/cisFaultInvalidShortNino');
const { app } = require('../test-helpers');

const API = '/cis/get/api-038-shared';

async function testApi038(nino, status, body) {
  const res = await request(app)
    .get(API)
    .set('nino', nino)
    .send({});
  expect(res.status)
    .toEqual(status);
  expect(res.body)
    .toEqual(body);
  return res;
}

describe('cis-api-038', () => {
  test('succeeds - not at risk', async () => {
    await testApi038('AE767818', 200, { state: 'not-at-risk' });
  });

  test('succeeds - at risk', async () => {
    const res = await testApi038('PT568952', 200, cisApi0038FailResponse('PT568952'));
    expect(res.body.getPersonResponse.person[0].otherDataGroup[0].indInterest)
      .toEqual('Y');
  });

  test('fails - invalid nino', async () => {
    await testApi038('PT568952X', 400, cisFaultInvalidShortNino);
    await testApi038('', 400, cisFaultInvalidShortNino);
  });
});
