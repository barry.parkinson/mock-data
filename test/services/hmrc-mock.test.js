const request = require('supertest');
const { app } = require('../test-helpers');

const VERIFY_URL = '/prove-your-identity/standalone-verification';
const SEARCH_URL = '/individuals/iv-orchestration/iv-sessiondata/search';

async function testHMRC(initialRequest, response) {
  const res = await request(app)
    .post(`${VERIFY_URL}?continueUrl=foobar`)
    .send(initialRequest);
  expect(res.status)
    .toEqual(302);
  const journeyId = res.headers.location.split('=')[1];
  // now search hmrc
  const res2 = await request(app)
    .post(SEARCH_URL)
    .send({ journeyId });
  expect(res2.body)
    .toEqual(response);
}

describe('hmrc-mock', () => {
  const usual = {
    nino: 'RJ428577B',
    firstName: 'Kyle',
    lastName: 'Scoby',
    dateOfBirth: '1967-11-23',
    postCode: 'TQ1 3UD',

  };

  test('success 2 evidence passed', async () => {
    await testHMRC({
      nino: 'RJ428577B',
      outcome: 'Success',
      evidences: 'passport,p60',
    }, {
      confidenceLevel: 250,
      evidencesPassedCount: 2,
      ivFailureReason: 'Success',
      ...usual,
    });
  });

  test('success 1 evidence passed', async () => {
    await testHMRC({
      nino: 'RJ428577B',
      outcome: 'Success',
      evidences: 'passport',
    }, {
      confidenceLevel: 250,
      evidencesPassedCount: 1,
      ivFailureReason: 'Success',
      ...usual,
    });
  });

  test('failed 0 evidence passed', async () => {
    await testHMRC({
      nino: 'RJ428577B',
      outcome: 'FailedIV',
      evidences: '',
    }, {
      confidenceLevel: 50,
      evidencesPassedCount: 0,
      ivFailureReason: 'User failed IV',
      ...usual,
    });
  });

  test('failed not found', async () => {
    await testHMRC({
      nino: 'AA000000A',
      outcome: 'Success',
      evidences: '',
    }, {
      confidenceLevel: 250,
      ivFailureReason: 'TechnicalIssue',
    });
  });

  test('get on verify url', async () => {
    const res = await request(app)
      .get(VERIFY_URL)
      .send({});
    expect(res.status)
      .toEqual(200);
  });

  test('get on test url', async () => {
    const res = await request(app)
      .get('/someurl')
      .send({});
    expect(res.status)
      .toEqual(200);
  });

  test('oauth token', async () => {
    const res = await request(app)
      .post('/oauth/token')
      .send({});
    expect(res.status)
      .toEqual(200);
  });
});
