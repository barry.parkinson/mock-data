const request = require('supertest');
const api014Response = require('../../app/data/responses/cisApi014Response');
const { app } = require('../test-helpers');

const API = '/cis/get/api-014-shared';

async function testApi014(nino, status, body) {
  const res = await request(app)
    .get(API)
    .set('nino', nino)
    .send({});
  expect(res.status)
    .toEqual(status);
  expect(res.body)
    .toEqual(body);
  return res;
}

describe('cis-api-014', () => {
  test('succeeds', async () => {
    await testApi014('RJ428577', 200, api014Response({ CIS: { services: '85,675' } }));
  });

  test('succeeds again', async () => {
    await testApi014('AE767818', 200, expect.any(Object)); // TODO define response
  });

  test('succeeds when user has no services', async () => {
    await testApi014('MW000137', 200, api014Response({ CIS: {} }));
  });

  test('succeeds with unknown person', async () => {
    await testApi014('QQ123456', 200, { err: 'NOT FOUND' });
  });
});
