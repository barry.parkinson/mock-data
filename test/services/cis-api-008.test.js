const request = require('supertest');
const { app } = require('../test-helpers');

const API = '/cis/get/api-008-shared';

async function testApi008(nino, status, body) {
  const res = await request(app)
    .get(API)
    .set('nino', nino)
    .send({});
  expect(res.status)
    .toEqual(status);
  expect(res.body)
    .toEqual(body);
  return res;
}

describe('cis-api-008', () => {
  test('succeeds', async () => {
    await testApi008('MW200002', 200, {
      GetContactDetailsResponse: {
        header: [{
          natSensInd: '1',
        }],
        contactDetails: [{
          contactTypeID: '1',
          detail: '01144960123',
        }, {
          contactTypeID: '3',
          detail: '00447700900123',
        }],
      },
    });
  });

  test('fails if not found', async () => {
    await testApi008('QQ000000', 200, { error: 'NOT FOUND' });
  });
});
