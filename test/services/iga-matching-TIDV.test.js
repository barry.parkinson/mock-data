const request = require('supertest');
const { app } = require('../test-helpers');

const API = '/match/';

async function testMatching(data, status, body, policy = 'POLICY_13') {
  const res = await request(app)
    .post(API)
    .set('policy-id', policy)
    .send(data);
  expect(res.body)
    .toEqual(body);
  expect(res.status)
    .toEqual(status);
  return res;
}

describe('iga-matching TIDV', () => {
  test('finds SCENARIO_1', async () => {
    await testMatching(
      {
        contactDetails: ['0428096805'],
        dateOfBirth: '1983-04-14',
      },
      200,
      {
        matchingScenario: 'SCENARIO_1',
        identifierValue: 'dbfba70f83735c0a21f4ad49094b37c1e05ae5ecf500e5c2d4296b46f69b1da3',
      },
    );
  });

  test('finds SCENARIO_2', async () => {
    await testMatching(
      {
        contactDetails: ['00919498936244'],
        postcode: 'LS14 1AA',
      },
      200,
      {
        matchingScenario: 'SCENARIO_2',
        identifierValue: '844440a0cdd53d763715724619987af0228675d58c0813ea53d3d1b699c0fddf',
      },
    );
  });

  test('finds SCENARIO_3', async () => {
    await testMatching(
      {
        postcode: 'LS12 3XX', // actual LS12 3NH
        contactDetails: ['9099014821'],
        nino: '7822',
      },
      200,
      {
        matchingScenario: 'SCENARIO_3',
        identifierValue: 'fcce9d49f72c6ec0cb8f1723f90709223e16b5fc6a40ceade81138d4613b490a',
      },
    );
  });

  test('requests postcode', async () => {
    await testMatching(
      {
        dateOfBirth: '1987-01-22', // 06
        contactDetails: ['9099014821'],
      },
      422,
      {
        statusCode: 422,
        status: 'UNPROCESSABLE_ENTITY',
        error: '2',
        message: 'Further details required. POSTCODE',
        attributeEnums: ['POSTCODE'],
        loggingIdentifierValue: '25f7134d-f5b5-48d0-83bb-bd9f9a98f88c',
      },
    );
  });

  test('requests further details and nino', async () => {
    await testMatching(
      {
        postcode: 'LS12 3XX', // actual LS12 3NH
        contactDetails: [
          '9099014821', '02071882329', '08003289393',
        ],
      },
      422,
      {
        statusCode: 422,
        status: 'UNPROCESSABLE_ENTITY',
        error: '2',
        message: 'Further details required. CONTACT_DETAILS NINO',
        attributeEnums: [
          'CONTACT_DETAILS',
          'NINO',
        ],
        loggingIdentifierValue: '25f7134d-f5b5-48d0-83bb-bd9f9a98f88c',
      },
    );
  });

  test('bad request', async () => {
    await testMatching(
      {
        contactDetails: [],
        postcode: 'LS1 XXX',
      },
      400,
      {
        statusCode: 400,
        status: 'BAD_REQUEST',
        message: 'Insufficient information supplied for match using policy: POLICY_13',
      },
    );
  });

  test('not found', async () => {
    await testMatching(
      {
        contactDetails: ['9999999999'],
        postcode: 'LS1 XXX',
      },
      422,
      {
        statusCode: 422,
        status: 'UNPROCESSABLE_ENTITY',
        error: '0',
        message: 'No results found.',
        loggingIdentifierValue: '61b14ee9-1b8a-4e02-80c9-7402711125e1',
      },
    );
  });
});
