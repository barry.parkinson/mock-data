const request = require('supertest');
const { app } = require('../test-helpers');

const API = '/match/';

async function testMatching(data, status, body, policy = 'POLICY_12') {
  const res = await request(app)
    .post(API)
    .set('policy-id', policy)
    .send(data);
  expect(res.body)
    .toEqual(body);
  expect(res.status)
    .toEqual(status);
  return res;
}

describe('iga-matching OIDV', () => {
  test('succeeds with exact match', async () => {
    await testMatching({
      forename: 'kyle',
      surname: 'SCOBY',
      dateOfBirth: '1967-11-23',
      postcode: 'TQ1 3UD',
    }, 200, {
      matchingScenario: 'SCENARIO_EXACT',
      identifierValue: '13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5',
    });
  });

  test('succeeds with SCENARIO_1', async () => {
    await testMatching({
      surname: 'Scoby',
      dateOfBirth: '1967-11-23',
      postcode: 'TQ1 3UD',
    }, 200, {
      matchingScenario: 'SCENARIO_1',
      identifierValue: '13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5',
    });
  });

  test('succeeds with SCENARIO_2', async () => {
    await testMatching({
      forename: 'Kyle',
      dateOfBirth: '1967-11-23',
      postcode: 'TQ1 3UD',
    }, 200, {
      matchingScenario: 'SCENARIO_2',
      identifierValue: '13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5',
    });
  });

  test('succeeds with SCENARIO_3', async () => {
    await testMatching({
      forename: 'Kyle',
      surname: 'scoby',
      dateOfBirth: '1967-11-23',
    }, 200, {
      matchingScenario: 'SCENARIO_3',
      identifierValue: '13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5',
    });
  });

  test('succeeds with SCENARIO_4', async () => {
    await testMatching({
      surname: 'Scoby',
      dateOfBirth: '1967-11-23',
      contactDetails: ['scoby@gmail.com'],
    }, 200, {
      matchingScenario: 'SCENARIO_4',
      identifierValue: '13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5',
    });
  });

  test('succeeds with SCENARIO_5', async () => {
    await testMatching({
      forename: 'KYLE',
      dateOfBirth: '1967-11-23',
      contactDetails: ['SCOBY@GMAIL.COM'],
    }, 200, {
      matchingScenario: 'SCENARIO_5',
      identifierValue: '13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5',
    });
  });

  test('returns not found', async () => {
    await testMatching({}, 422, {
      statusCode: 422,
      status: 'UNPROCESSABLE_ENTITY',
      error: '0',
      message: 'No results found.',
      loggingIdentifierValue: '51b14ee9-1b8a-4e02-80c9-7402711125e1',
    });
  });

  test('requires further information', async () => {
    await testMatching({
      surname: 'Scoby',
      dateOfBirth: '1967-11-23',
    }, 422, {
      statusCode: 422,
      status: 'UNPROCESSABLE_ENTITY',
      error: '2',
      message: 'Further details required. POSTCODE',
      attributeEnums: ['POSTCODE'],
      loggingIdentifierValue: '51b14ee9-1b8a-4e02-80c9-7402711125e1',
    });
  });

  test('rejects invalid policy id', async () => {
    await testMatching(
      {},
      400,
      {
        statusCode: 400,
        status: 'BAD_REQUEST',
        message: 'Unknown Policy Id supplied in headers',
      },
      'foo',
    );
  });
});
