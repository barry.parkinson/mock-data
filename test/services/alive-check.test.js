const request = require('supertest');
const { app } = require('../test-helpers');

const ALIVE_URL = '/integration/aliveCheck-shared';

async function testAliveCheck(nino, status, body) {
  const res = await request(app)
    .get(ALIVE_URL)
    .set('nino', nino)
    .send({});
  expect(res.status)
    .toEqual(status);
  expect(res.body)
    .toEqual(body);
  return res;
}

describe('alive-check', () => {
  test('succeeds', async () => {
    await testAliveCheck('RJ428577', 200, {
      data: {
        isAlive: true,
      },
    });
  });

  test('NINO PT568951 fails', async () => {
    await testAliveCheck('PT568951', 200, {
      data: {
        isAlive: false,
        death: {
          date: '2011-10-11',
          description: 'Verified to Level 3',
          verification: 3,
        },
      },
    });
  });

  test('NINO not found', async () => {
    await testAliveCheck('QQ123456', 404, {
      error: {
        httpStatusCode: '404',
        message: 'NINO Not Found',
      },
    });
  });

  test('NINO not valid', async () => {
    await testAliveCheck('ABC12345', 422, {
      // correlationId,
      error: {
        errors: [{
          code: '2',
          message: 'Supplied NINO is invalid - format must be AA999999.',
        }],
        httpStatusCode: '422',
        message: 'Validation error(s)',
      },
    });
  });
});
