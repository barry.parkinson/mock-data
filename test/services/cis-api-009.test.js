const request = require('supertest');
const { app } = require('../test-helpers');

const API = '/cis/get/api-009-shared';

async function testApi009(nino, status, body) {
  const res = await request(app)
    .get(API)
    .set('nino', nino)
    .send({});
  expect(res.status)
    .toEqual(status);
  expect(res.body)
    .toEqual(body);
  return res;
}

describe('cis-api-009', () => {
  test('succeeds', async () => {
    await testApi009('RJ428577', 200, {
      GetRelationshipDetailsResponse: {
        header: [{
          natSensInd: '1',
        }],
        personRoleRelationships: [
          {
            personName: [
              {
                forenames: 'Claire',
                surname: 'Chapman',
              },
            ],
            birthDate: '23-Mar-1990',
            roleRelationshipTypeID: '2',
            relDWPServiceKey: '10',
            sourceFlag: '1',
            nino: 'MW000199',
            ninoSuffix: 'A',
          },
          {
            personName: [
              {
                forenames: 'Ella',
                surname: 'Chapman',
              },
            ],
            birthDate: '01-Feb-2007',
            roleRelationshipTypeID: '3',
            relDWPServiceKey: '85',
            sourceFlag: '1',
            nino: '',
            ninoSuffix: 'A',
          },
          {
            personName: [
              {
                forenames: 'Charlie',
                surname: 'Chapman',
              },
            ],
            birthDate: '01-Feb-2009',
            roleRelationshipTypeID: '3',
            relDWPServiceKey: '6',
            sourceFlag: '1',
            nino: '',
            ninoSuffix: 'A',
          },
        ],
      },
    });
  });

  test('fails if not found', async () => {
    await testApi009('QQ000000', 404, { error: 'NF' });
  });
});
