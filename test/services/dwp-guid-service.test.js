const request = require('supertest');
const { app } = require('../test-helpers');

const GUID_TO_NINO = '/dwp-guid-service/v0.2/NINO/for/DWP-GUID/';
const NINO_TO_GUID = '/dwp-guid-service/v0.2/DWP-GUID/for/NINO/';

async function testGuidToNino(guid, status, body) {
  const res = await request(app)
    .get(GUID_TO_NINO + guid)
    .send({});
  expect(res.status)
    .toEqual(status);
  expect(res.body)
    .toEqual(body);
  return res;
}

async function testNinoToGuid(nino, status, body) {
  const res = await request(app)
    .get(NINO_TO_GUID + nino)
    .send({});
  expect(res.status)
    .toEqual(status);
  expect(res.body)
    .toEqual(body);
  return res;
}

describe('dwp-guid-service', () => {
  test.each([
    ['13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5', 'RJ428577B'],
    ['dbfba70f83735c0a21f4ad49094b37c1e05ae5ecf500e5c2d4296b46f69b1da3', 'GN129520A'],
    ['b7d6f25ec0f33222c3dadd8e2bcd5ceb8959438bdaf42649bae5365c9e9a1bf3', 'AE767817B'],
    ['844440a0cdd53d763715724619987af0228675d58c0813ea53d3d1b699c0fddf', 'MW200002A'],
    ['9f9f46d8b2115fc66510020863a182abf3c03b0501a1b3bde5d88f089a4d2857', 'MW000137A'],
  ])('converts GUID %p to NINO %p', async (guid, nino) => {
    await testGuidToNino(guid, 200, {
      identifier: nino,
      type: 'NINO',
    });
  });

  test('fails on an unknown guid', async () => {
    await testGuidToNino('aaa03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc7aaa', 422, {
      status: '422 UNPROCESSABLE_ENTITY',
      messages: [
        'Supplied DWP_GUID not found',
      ],
    });
  });

  test('fails on an invalid guid', async () => {
    await testGuidToNino('invalid', 400, {
      status: '400 BAD_REQUEST',
      messages: [
        'Input identifier not valid',
      ],
    });
  });

  test.each([
    ['13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5', 'RJ428577B'],
    ['dbfba70f83735c0a21f4ad49094b37c1e05ae5ecf500e5c2d4296b46f69b1da3', 'GN129520A'],
    ['b7d6f25ec0f33222c3dadd8e2bcd5ceb8959438bdaf42649bae5365c9e9a1bf3', 'AE767817B'],
    ['844440a0cdd53d763715724619987af0228675d58c0813ea53d3d1b699c0fddf', 'MW200002A'],
    ['9f9f46d8b2115fc66510020863a182abf3c03b0501a1b3bde5d88f089a4d2857', 'MW000137A'],
  ])('gets GUID %p from NINO %p', async (guid, nino) => {
    await testNinoToGuid(nino, 200, {
      identifier: guid,
      type: 'DWP_GUID',
    });
  });

  test('fails on an unknown nino', async () => {
    await testNinoToGuid('AB000000B', 422, {
      status: '422 UNPROCESSABLE_ENTITY',
      messages: [
        'Supplied NINO not found',
      ],
    });
  });

  test('fails on an invalid nino', async () => {
    await testNinoToGuid('AA0000', 400, {
      status: '400 BAD_REQUEST',
      messages: [
        'Input identifier not valid',
      ],
    });
  });
});
