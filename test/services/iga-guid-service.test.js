const request = require('supertest');
const { app } = require('../test-helpers');

const GUID_SERVICE = '/guid-service';

async function testGuidService(guid, status, body) {
  const res = await request(app)
    .get(GUID_SERVICE)
    .set('dwp-guid', guid)
    .send({});
  expect(res.status)
    .toEqual(status);
  expect(res.body)
    .toEqual(body);
  return res;
}

describe('iga-guid-service', () => {
  test.each([
    ['13f03f9da3a0f493e04df091865f8e77f636ac211948e10ec94e279392bc75a5', 'RJ428577B'],
    ['dbfba70f83735c0a21f4ad49094b37c1e05ae5ecf500e5c2d4296b46f69b1da3', 'GN129520A'],
    ['b7d6f25ec0f33222c3dadd8e2bcd5ceb8959438bdaf42649bae5365c9e9a1bf3', 'AE767817B'],
    ['844440a0cdd53d763715724619987af0228675d58c0813ea53d3d1b699c0fddf', 'MW200002A'],
    ['9f9f46d8b2115fc66510020863a182abf3c03b0501a1b3bde5d88f089a4d2857', 'MW000137A'],
  ])('GUID %p maps to %p', async (guid, nino) => {
    await testGuidService(guid, 200, {
      data: {
        identifier: nino,
        type: 'NINO',
      },
    });
  });

  test('Fails on an unknown guid', async () => {
    await testGuidService('no-such-guid', 422, {
      status: '422 UNPROCESSABLE_ENTITY',
      messages: [
        'Supplied DWP_GUID not found',
      ],
    });
  });
});
