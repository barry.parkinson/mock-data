const request = require('supertest');
const { app } = require('../test-helpers');

describe('Application', () => {
  test('returns 404 on unknown routes', async () => {
    console.log = jest.fn(); // eslint-disable-line no-console
    const res = await request(app)
      .post('/unknown-route')
      .send({});
    expect(res.status)
      .toEqual(404);
  });

  test('returns 500 on invalid json sent', async () => {
    console.error = jest.fn(); // eslint-disable-line no-console
    const res = await request(app)
      .post('/any')
      .set('content-type', 'application/json')
      .send('{ invalid:');
    expect(res.status)
      .toEqual(500);
  });

  test('returns html page on /mock-info route', async () => {
    const res = await request(app)
      .get('/mock-info')
      .send();
    expect(res.status)
      .toEqual(200);
    expect(res.text)
      .toContain('<h1>Mock Data v1.0.0</h1>');
  });

  test('returns html page on /mock-endpoints route', async () => {
    const res = await request(app)
      .get('/mock-endpoints')
      .send();
    expect(res.status)
      .toEqual(200);
    expect(res.text)
      .toContain('<h1>Mock Data v1.0.0 - Endpoints</h1>');
  });

  test('returns html page on /api/info/alive-check route', async () => {
    const res = await request(app)
      .get('/api/info/alive-check')
      .send();
    expect(res.status)
      .toEqual(200);
    expect(res.text)
      .toContain('<h1>Endpoint: Alive Check</h1>');
  });

  test('returns html page on /api/info/match-tidv', async () => {
    const res = await request(app)
      .get('/api/info/match-tidv')
      .send();
    expect(res.status)
      .toEqual(200);
    expect(res.text)
      .toContain('<h1>Endpoint: TIDV FINDr Matching</h1>');
    expect(res.text)
      .toContain('<p>Body:');
  });

  test('returns html page on /api/info/dwp-guid-from-nino', async () => {
    const res = await request(app)
      .get('/api/info/dwp-guid-from-nino')
      .send();
    expect(res.status)
      .toEqual(200);
    expect(res.text)
      .toContain('<h1>Endpoint: DWP Guid Service - get Guid from NINO</h1');
    expect(res.text)
      .not
      .toContain('<p>Body:');
  });

  test('returns 404 on unknown api', async () => {
    const res = await request(app)
      .get('/api/info/unknown')
      .send();
    expect(res.status)
      .toEqual(404);
    expect(res.text)
      .toContain('Not Found');
  });

  test('returns person details', async () => {
    const res = await request(app)
      .get('/mock-detail/nino/AE767806A')
      .send();
    expect(res.status)
      .toEqual(200);
    expect(res.text)
      .toContain('"guid": "dfc551c813a34ae7c17773cb809e54b7a23729dd875d72defd93418b81cf39e1"');
  });

  test('returns person details not found', async () => {
    const res = await request(app)
      .get('/mock-detail/nino/QQ123456Z')
      .send();
    expect(res.status)
      .toEqual(200);
    expect(res.body)
      .toEqual({ nino: 'QQ123456Z', error: 'not found' });
  });

  // TODO move the PIP tests

  test('returns 404 on PIP route', async () => {
    const res = await request(app)
      .post('/PIPGetEntitlement')
      .set('content-type', 'application/xml')
      .send('<xml>test</xml>');
    expect(res.status)
      .toEqual(404);
  });

  test('returns 404 on PIP route with NINO', async () => {
    const res = await request(app)
      .post('/PIPGetPersonalNewDetails')
      .set('content-type', 'application/xml')
      .send('<xml>test<NINO>AE767824C</NINO></xml>');
    expect(res.status)
      .toEqual(404);
  });
});
